/*
SQLyog Enterprise v12.09 (64 bit)
MySQL - 10.4.8-MariaDB : Database - aw
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`aw` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `aw`;

/*Table structure for table `bkeluar` */

DROP TABLE IF EXISTS `bkeluar`;

CREATE TABLE `bkeluar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` date DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `cabang` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;

/*Table structure for table `bmasuk` */

DROP TABLE IF EXISTS `bmasuk`;

CREATE TABLE `bmasuk` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` date DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_user` (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8mb4;

/*Table structure for table `brusak` */

DROP TABLE IF EXISTS `brusak`;

CREATE TABLE `brusak` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` date DEFAULT NULL,
  `keterangan` varchar(50) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `is_accept` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `id_user` (`id_user`),
  CONSTRAINT `brusak_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4;

/*Table structure for table `detail_bkeluar` */

DROP TABLE IF EXISTS `detail_bkeluar`;

CREATE TABLE `detail_bkeluar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_bkeluar` int(11) DEFAULT NULL,
  `id_barang` int(11) DEFAULT NULL,
  `jumlah` int(5) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_bkeluar` (`id_bkeluar`),
  KEY `id_barang` (`id_barang`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4;

/*Table structure for table `detail_bmasuk` */

DROP TABLE IF EXISTS `detail_bmasuk`;

CREATE TABLE `detail_bmasuk` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_bmasuk` int(11) DEFAULT NULL,
  `id_barang` int(11) DEFAULT NULL,
  `jumlah` int(5) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_bmasuk` (`id_bmasuk`),
  KEY `id_barang` (`id_barang`)
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=utf8mb4;

/*Table structure for table `detail_brusak` */

DROP TABLE IF EXISTS `detail_brusak`;

CREATE TABLE `detail_brusak` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_brusak` int(11) DEFAULT NULL,
  `id_barang` int(11) DEFAULT NULL,
  `jumlah` int(5) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_brusak` (`id_brusak`),
  KEY `id_barang` (`id_barang`),
  CONSTRAINT `detail_brusak_ibfk_1` FOREIGN KEY (`id_brusak`) REFERENCES `brusak` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4;

/*Table structure for table `detail_pgudang` */

DROP TABLE IF EXISTS `detail_pgudang`;

CREATE TABLE `detail_pgudang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_pgudang` int(11) DEFAULT NULL,
  `id_barang` int(11) DEFAULT NULL,
  `jumlah` int(5) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_pgudang` (`id_pgudang`),
  KEY `id_barang` (`id_barang`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4;

/*Table structure for table `detail_psupplier` */

DROP TABLE IF EXISTS `detail_psupplier`;

CREATE TABLE `detail_psupplier` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_psupplier` int(11) DEFAULT NULL,
  `id_barang` int(11) DEFAULT NULL,
  `jumlah` int(5) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_psupplier` (`id_psupplier`),
  KEY `id_barang` (`id_barang`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4;

/*Table structure for table `evaluasi` */

DROP TABLE IF EXISTS `evaluasi`;

CREATE TABLE `evaluasi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_manager` int(11) DEFAULT NULL,
  `penjelasan` text DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `is_read` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `id_manager` (`id_manager`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;

/*Table structure for table `pgudang` */

DROP TABLE IF EXISTS `pgudang`;

CREATE TABLE `pgudang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` date DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `is_read` tinyint(1) DEFAULT 0,
  `cabang` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_user` (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4;

/*Table structure for table `produk` */

DROP TABLE IF EXISTS `produk`;

CREATE TABLE `produk` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) DEFAULT NULL,
  `jumlah` int(5) DEFAULT 0,
  `satuan` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;

/*Table structure for table `psupplier` */

DROP TABLE IF EXISTS `psupplier`;

CREATE TABLE `psupplier` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` date DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_user` (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(128) NOT NULL,
  `jk` enum('Pria','Wanita') NOT NULL,
  `status_nikah` enum('Sudah','Belum') NOT NULL,
  `foto` text NOT NULL,
  `username` varchar(128) NOT NULL,
  `password` text NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/* Trigger structure for table `detail_bkeluar` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `bkeluar_insert` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'localhost' */ /*!50003 TRIGGER `bkeluar_insert` AFTER INSERT ON `detail_bkeluar` FOR EACH ROW BEGIN
	UPDATE produk SET jumlah=(SELECT jumlah FROM produk WHERE id=new.id_barang)-new.jumlah WHERE id=new.id_barang;
    END */$$


DELIMITER ;

/* Trigger structure for table `detail_bkeluar` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `bkeluar_delete` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'localhost' */ /*!50003 TRIGGER `bkeluar_delete` AFTER DELETE ON `detail_bkeluar` FOR EACH ROW BEGIN
	UPDATE produk SET jumlah=(SELECT jumlah FROM produk WHERE id=old.id_barang)+old.jumlah WHERE id=old.id_barang;
    END */$$


DELIMITER ;

/* Trigger structure for table `detail_bmasuk` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `bmasuk_insert` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'localhost' */ /*!50003 TRIGGER `bmasuk_insert` AFTER INSERT ON `detail_bmasuk` FOR EACH ROW BEGIN
	update produk set jumlah=(select jumlah from produk where id=new.id_barang)+new.jumlah where id=new.id_barang;
    END */$$


DELIMITER ;

/* Trigger structure for table `detail_bmasuk` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `bmasuk_delete` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'localhost' */ /*!50003 TRIGGER `bmasuk_delete` AFTER DELETE ON `detail_bmasuk` FOR EACH ROW BEGIN
	UPDATE produk SET jumlah=(SELECT jumlah FROM produk WHERE id=old.id_barang)-old.jumlah WHERE id=old.id_barang;
    END */$$


DELIMITER ;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
