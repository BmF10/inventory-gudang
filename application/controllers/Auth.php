<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{
    public function index()
    {
        if ($this->session->userdata('username')) {
            redirect('pegawaigudang/home');
        }

        $this->form_validation->set_rules(
            'username',
            'Username',
            'trim|required|valid_email',
            [
                'required' => '<i class="fa fa-info-circle"></i> <i>Username</i> tidak boleh kosong',
                'valid_email' => '<i class="fa fa-info-circle"></i> Format Penulisan Email Anda Salah'
            ]
        );
        $this->form_validation->set_rules(
            'pass',
            'Password',
            'trim|required',
            [
                'required' => '<i class="fa fa-info-circle"></i> <i>Password</i> tidak boleh kosong'
            ]
        );

        if ($this->form_validation->run() == false) {
            $this->load->view('auth/login');
        } else {
            //validasi success
            $this->_login();
        }
    }

    private function _login()
    {
        $username = $this->input->post('username'); //lolos validasi
        $password = $this->input->post('pass');

        $user = $this->db->get_where('user', ['username' => $username])->row_array();

        if ($user) {
            if (password_verify($password, $user['password'])) {
                $data = [
                    'user_id' => $user['id'],
                    'username' => $user['username'],
                    'role_id' => $user['role_id']
                ];
                $this->session->set_userdata($data); // simpan ke dalam session
                if ($user['role_id'] == 1) {
                    redirect('managerutama/home');
                    // redirect('ManajerUtama');
                } elseif ($user['role_id'] == 2) {
                    redirect('managergudang/home');
                } else {
                    redirect('pegawaigudang/home');
                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Password salah!</div>');
                redirect('auth');
            }
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Akun belum terdaftar!</div>');
            redirect('auth');
        }
    }

    public function logout()
    {
        $this->session->unset_userdata('user_id');
        $this->session->unset_userdata('username');
        $this->session->unset_userdata('role_id');
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil log out!</div>');
        redirect('auth');
    }
}
