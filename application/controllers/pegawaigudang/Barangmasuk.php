<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Barangmasuk extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        // $this->load->library('form_validation');
        $this->load->model('Model_bmasuk');
        is_logged_in();
        pg_only();
    }

    public function index()
    {
        $data = [
            'title' => 'Pegawai Gudang',
            'user'  => $this->db->get_where('user', ['username' => $this->session->userdata('username')])->row_array(),
            'bmasuk' => $this->Model_bmasuk->getAllData()->result(),
            'menu_active'   => '#menu-pegawai-masuk',
        ];
        $this->template->load('AdminLTE', 'pegawai-gudang/barangmasuk', $data);
    }

    public function add()
    {
        $data = [
            'title' => 'Pegawai Gudang',
            'user'  => $this->db->get_where('user', ['username' => $this->session->userdata('username')])->row_array(),
            'barang' => $this->db->get('produk')->result(),
            'menu_active'   => '#menu-pegawai-masuk',
        ];
        $this->template->load('AdminLTE', 'pegawai-gudang/barangmasukadd', $data);
    }

    public function addbarang()
    {
        $data = [
            'tanggal' => date("Y-m-d H:i:s"),
            'id_user' => $this->session->userdata('user_id')
        ];

        $this->db->insert('bmasuk', $data);
        $insert_id = $this->db->insert_id();

        $json = [
            'insert_id' => $insert_id,
            'msg' => 'success',
            'data' => $this->input->post()
        ];

        $data1 = [
            'id_bmasuk' => $insert_id,
            'id_barang' => $this->input->post('id_barang'),
            'jumlah' => $this->input->post('jumlah'),
        ];

        $this->db->insert('detail_bmasuk', $data1);

        echo json_encode($json);
    }

    public function addbarangnext()
    {
        $data = [
            'id_bmasuk' => $this->input->post('id_bmasuk'),
            'id_barang' => $this->input->post('id_barang'),
            'jumlah' => $this->input->post('jumlah'),
        ];

        $this->db->insert('detail_bmasuk', $data);

        echo json_encode(['msg' => 'success']);
    }

    public function barang($id_bmasuk)
    {
        $data = $this->Model_bmasuk->getData($id_bmasuk)->result();

        echo json_encode($data);
    }

    public function deletebarang($id_detail)
    {
        $this->db->delete('detail_bmasuk', ['id' => $id_detail]);
    }

    public function detail($id)
    {
        $data = [
            'title' => 'Pegawai Gudang',
            'user'  => $this->db->get_where('user', ['username' => $this->session->userdata('username')])->row_array(),
            'detail' => $this->Model_bmasuk->getDetail($id)->row(),
            'detail2' => $this->Model_bmasuk->getDetail($id)->result(),
            'jumlah' => $this->Model_bmasuk->getDetail($id)->num_rows(),
            'menu_active'   => '#menu-pegawai-masuk',
        ];
        $this->template->load('AdminLTE', 'pegawai-gudang/barangmasukdetail', $data);
    }
}
