<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Produk extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        // $this->load->library('form_validation');
        $this->load->model('Model_brusak');
        is_logged_in();
        pg_only();
    }

    public function index()
    {
        $data = [
            'title' => 'Pegawai Gudang',
            'user'  => $this->db->get_where('user', ['username' => $this->session->userdata('username')])->row_array(),
            'produk' => $this->db->get('produk')->result(),
            'menu_active'   => '#menu-data-produk',
        ];
        $this->template->load('AdminLTE', 'pegawai-gudang/produk', $data);
    }
}
