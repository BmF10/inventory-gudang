<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Barangkeluar extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        // $this->load->library('form_validation');
        $this->load->model('Model_bkeluar');
        is_logged_in();
        pg_only();
    }

    public function index()
    {
        $data = [
            'title' => 'Pegawai Gudang',
            'user'  => $this->db->get_where('user', ['username' => $this->session->userdata('username')])->row_array(),
            'bkeluar' => $this->Model_bkeluar->getAllData()->result(),
            'menu_active'   => '#menu-pegawai-keluar',
        ];
        $this->template->load('AdminLTE', 'pegawai-gudang/barangkeluar', $data);
    }

    public function add()
    {
        $data = [
            'title' => 'Pegawai Gudang',
            'user'  => $this->db->get_where('user', ['username' => $this->session->userdata('username')])->row_array(),
            'barang' => $this->db->get('produk')->result(),
            'menu_active'   => '#menu-pegawai-keluar',
        ];
        $this->template->load('AdminLTE', 'pegawai-gudang/barangkeluaradd', $data);
    }

    public function addbarang()
    {
        $data = [
            'tanggal' => date("Y-m-d H:i:s"),
            'id_user' => $this->session->userdata('user_id'),
            'cabang' => $this->input->post('cabang'),
        ];

        $this->db->insert('bkeluar', $data);
        $insert_id = $this->db->insert_id();

        $json = [
            'insert_id' => $insert_id,
            'msg' => 'success',
            'data' => $this->input->post()
        ];

        $data1 = [
            'id_bkeluar' => $insert_id,
            'id_barang' => $this->input->post('id_barang'),
            'jumlah' => $this->input->post('jumlah'),
        ];

        $this->db->insert('detail_bkeluar', $data1);

        echo json_encode($json);
    }

    public function addbarangnext()
    {
        $data = [
            'id_bkeluar' => $this->input->post('id_bkeluar'),
            'id_barang' => $this->input->post('id_barang'),
            'jumlah' => $this->input->post('jumlah'),
        ];

        $this->db->insert('detail_bkeluar', $data);

        echo json_encode(['msg' => 'success']);
    }

    public function barang($id_bkeluar)
    {
        $data = $this->Model_bkeluar->getData($id_bkeluar)->result();

        echo json_encode($data);
    }

    public function deletebarang($id_detail)
    {
        $this->db->delete('detail_bkeluar', ['id' => $id_detail]);
    }

    public function detail($id)
    {
        $data = [
            'title' => 'Pegawai Gudang',
            'user'  => $this->db->get_where('user', ['username' => $this->session->userdata('username')])->row_array(),
            'detail' => $this->Model_bkeluar->getDetail($id)->row(),
            'detail2' => $this->Model_bkeluar->getDetail($id)->result(),
            'jumlah' => $this->Model_bkeluar->getDetail($id)->num_rows(),
            'menu_active'   => '#menu-pegawai-keluar',
        ];
        $this->template->load('AdminLTE', 'pegawai-gudang/barangkeluardetail', $data);
    }
}
