<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pemesanangudang extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        // $this->load->library('form_validation');
        is_logged_in();
        pg_only();
        $this->load->model('Model_pegawaigudang');
    }

    public function index()
    {
        $data = [
            'title' => 'Pegawai Gudang',
            'user'  => $this->db->get_where('user', ['username' => $this->session->userdata('username')])->row_array(),
            'menu_active'   => '#menu-pegawai-pemesanan',
            'pemesanan' => $this->Model_pegawaigudang->getAllPemesanan1()->result(),
        ];
        $this->template->load('AdminLTE', 'pegawai-gudang/pemesanangudang', $data);
    }
}
