<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Barangrusak extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        // $this->load->library('form_validation');
        $this->load->model('Model_brusak');
        is_logged_in();
        pg_only();
    }

    public function index()
    {
        $data = [
            'title' => 'Pegawai Gudang',
            'user'  => $this->db->get_where('user', ['username' => $this->session->userdata('username')])->row_array(),
            'brusak' => $this->Model_brusak->getAllData()->result(),
            'menu_active'   => '#menu-pegawai-rusak',
        ];
        $this->template->load('AdminLTE', 'pegawai-gudang/barangrusak', $data);
    }

    public function add()
    {
        $data = [
            'title' => 'Pegawai Gudang',
            'user'  => $this->db->get_where('user', ['username' => $this->session->userdata('username')])->row_array(),
            'barang' => $this->db->get('produk')->result(),
            'menu_active'   => '#menu-pegawai-rusak',
        ];
        $this->template->load('AdminLTE', 'pegawai-gudang/barangrusakadd', $data);
    }

    public function addbarang()
    {
        $data = [
            'tanggal' => date("Y-m-d H:i:s"),
            'id_user' => $this->session->userdata('user_id'),
            'keterangan' => $this->input->post('keterangan')
        ];

        $this->db->insert('brusak', $data);
        $insert_id = $this->db->insert_id();

        $json = [
            'insert_id' => $insert_id,
            'msg' => 'success',
            'data' => $this->input->post()
        ];

        $data1 = [
            'id_brusak' => $insert_id,
            'id_barang' => $this->input->post('id_barang'),
            'jumlah' => $this->input->post('jumlah'),
        ];

        $this->db->insert('detail_brusak', $data1);

        echo json_encode($json);
    }

    public function addbarangnext()
    {
        $data = [
            'id_brusak' => $this->input->post('id_brusak'),
            'id_barang' => $this->input->post('id_barang'),
            'jumlah' => $this->input->post('jumlah'),
        ];

        $this->db->insert('detail_brusak', $data);

        echo json_encode(['msg' => 'success']);
    }

    public function barang($id_brusak)
    {
        $data = $this->Model_brusak->getData($id_brusak)->result();

        echo json_encode($data);
    }

    public function deletebarang($id_detail)
    {
        $this->db->delete('detail_brusak', ['id' => $id_detail]);
    }

    public function detail($id)
    {
        $data = [
            'title' => 'Pegawai Gudang',
            'user'  => $this->db->get_where('user', ['username' => $this->session->userdata('username')])->row_array(),
            'detail' => $this->Model_brusak->getDetail($id)->row(),
            'detail2' => $this->Model_brusak->getDetail($id)->result(),
            'jumlah' => $this->Model_brusak->getDetail($id)->num_rows(),
            'menu_active'   => '#menu-pegawai-rusak',
        ];
        $this->template->load('AdminLTE', 'pegawai-gudang/barangrusakdetail', $data);
    }
}
