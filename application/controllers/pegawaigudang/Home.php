<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        // $this->load->library('form_validation');
        is_logged_in();
        pg_only();
        $this->load->model('Model_pegawaigudang');
    }

    public function index()
    {
        $data = [
            'title' => 'Pegawai Gudang',
            'user'  => $this->db->get_where('user', ['username' => $this->session->userdata('username')])->row_array(),
            'menu_active'   => '#menu-pegawai',
            'pemesanan' => $this->Model_pegawaigudang->getAllPemesanan()->result(),
            'evaluasi' => $this->Model_pegawaigudang->getAllEvaluasi()->result()
        ];
        $this->template->load('AdminLTE', 'pegawai-gudang/index', $data);
    }

    public function detail($id)
    {
        $data = [
            'title' => 'Pegawai Gudang',
            'user'  => $this->db->get_where('user', ['username' => $this->session->userdata('username')])->row_array(),
            'menu_active'   => '#menu-pegawai-pemesanan',
            'pemesanan' => $this->Model_pegawaigudang->getPemesanan($id)->row(),
            'pemesanan1' => $this->Model_pegawaigudang->getPemesanan($id)->result(),
            'jumlah_barang' => $this->Model_pegawaigudang->getPemesanan($id)->num_rows()
        ];
        $this->template->load('AdminLTE', 'pegawai-gudang/detailpemesanan', $data);
    }

    public function proses($id)
    {
        $this->db->update('pgudang', ['is_read' => '1'], ['id' => $id]);
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Data berhasil diubah!</div>');
        redirect('pegawaigudang/home');
    }

    public function detailevaluasi($id)
    {
        $data = [
            'title' => 'Pegawai Gudang',
            'user'  => $this->db->get_where('user', ['username' => $this->session->userdata('username')])->row_array(),
            'menu_active'   => '#menu-pegawai',
            'evaluasi' => $this->Model_pegawaigudang->getEvaluasi($id)->row(),
        ];
        $this->template->load('AdminLTE', 'pegawai-gudang/detailevaluasi', $data);
    }

    public function readevaluasi($id)
    {
        $this->db->update('evaluasi', ['is_read' => '1'], ['id' => $id]);
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Data berhasil diubah!</div>');
        redirect('pegawaigudang/home');
    }
}
