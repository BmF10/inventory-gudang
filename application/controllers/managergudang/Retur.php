<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Retur extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        // $this->load->library('form_validation');
        is_logged_in();
        mg_only();
        $this->load->model('Model_retur');
    }

    public function index()
    {
        $data['title'] = "Retur Barang";
        $data['menu_active'] = "#retur";
        $data['data'] = $this->Model_retur->getData()->result();
        $data['user'] = $this->db->get_where('user', ['username' => $this->session->userdata('username')])->row_array();
        $this->template->load('AdminLTE', 'manajer-gudang/retur', $data);
    }

    public function detail($id)
    {
        $data = [
            'title' => 'Pegawai Gudang',
            'user'  => $this->db->get_where('user', ['username' => $this->session->userdata('username')])->row_array(),
            'detail' => $this->Model_retur->getDetail($id)->row(),
            'detail2' => $this->Model_retur->getDetail($id)->result(),
            'menu_active'   => '#retur',
        ];
        $this->template->load('AdminLTE', 'manajer-gudang/returdetail', $data);
    }

    public function accept($id)
    {
        $this->db->update('brusak', ['is_accept' => '1'], ['id' => $id]);
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Retur berhasil diterima.</div>');
        redirect('managergudang/retur');
    }

    public function reject($id)
    {
        $this->db->update('brusak', ['is_accept' => '2'], ['id' => $id]);
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Retur berhasil ditolak.</div>');
        redirect('managergudang/retur');
    }
}
