<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Evaluasi extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        // $this->load->library('form_validation');
        $this->load->model('Model_evaluasi');
        is_logged_in();
        mg_only();
    }

    public function index()
    {
        $data = [
            'title' => 'Manajer Utama',
            'user'  => $this->db->get_where('user', ['username' => $this->session->userdata('username')])->row_array(),
            'evaluasi' => $this->Model_evaluasi->getEvaluasi()->result(),
            'menu_active'   => '#menu-manajer-gudang-evaluasi',
        ];
        $this->template->load('AdminLTE', 'manajer-gudang/evaluasi', $data);
    }

    public function add()
    {
        $this->db->insert('evaluasi', $this->input->post());
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Data berhasil ditambahkan!</div>');
        redirect('managergudang/evaluasi');
    }

    public function delete($id)
    {
        $this->db->delete('evaluasi', ['id' => $id]);
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Data berhasil dihapus!</div>');
        redirect('managergudang/evaluasi');
    }
}
