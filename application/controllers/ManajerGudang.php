<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ManajerGudang extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        // $this->load->library('form_validation');
        is_logged_in();
        mg_only();
    }

    public function index()
    {
        $data = [
            'title' => 'Manajer Utama',
            'user'  => $this->db->get_where('user', ['username' => $this->session->userdata('username')])->row_array(),
            'menu_active'   => '#menu-manajer-utama',
        ];
        $this->template->load('AdminLTE', 'manajer-utama/index', $data);
    }
}
