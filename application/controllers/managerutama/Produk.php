<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Produk extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        // $this->load->library('form_validation');
        is_logged_in();
        mu_only();
        $this->load->model('Model_produk', 'produk');
    }
    public function index() //list data produk
    {
        $data = [
            'title'    => 'List Produk',
            'menu_active' => '#produk-active',
            'user'    => $this->db->get_where('user', ['username' => $this->session->userdata('username')])->row_array(),
            'produks' => $this->produk->getProduk()

        ];
        // var_dump($data['produks']); die;
        $this->template->load('AdminLTE', 'produk/index', $data);
    }

    public function addProduk()
    {
        //$data = $this->input->post();
        $data = [
            'nama' => $this->input->post('nama'),
            'satuan' => $this->input->post('satuan')
        ];
        $this->db->insert('produk', $data);

        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil menambahkan produk!</div>');
        redirect('managerutama/produk');
    }

    public function delProduk($id_produk)
    {
        $this->db->delete('produk', ['id' => $id_produk]);
        $this->session->set_flashdata(
            'message',
            '<div class="alert alert-success alert-dismissible">
                							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                							<h4><i class="nav-icon fas fa-warning"></i> Success!</h4>
                							Deleted produk !!!
										</div>'
        );
        redirect('managerutama/produk');
    }


    public function editProduk($id_produk = null)
    {
        $getId = $this->db->get_where('produk', ['id' => $id_produk])->row_array();
        $this->form_validation->set_rules('nama', 'Nama', 'required|trim');
        $this->form_validation->set_rules('satuan', 'Satuan', 'required|trim');
        $data = [
            'produk_id' => $getId,
            'title' => 'Form Edit Produk',
            'menu_active'   => '#produk-active',
            // 'error' => $error['error'],
            'user'  => $this->db->get_where('user', ['username' => $this->session->userdata('username')])->row_array(),
        ];

        if ($this->form_validation->run() == false) {
            $this->template->load('AdminLTE', 'produk/edit_produk', $data);
        } else {
            $nama =  $this->input->post('nama', true);
            $satuan =  $this->input->post('satuan', true);
            $get_id =  $this->input->post('id', true);

            $this->db->set('nama', $nama);
            $this->db->set('satuan', $satuan);
            $this->db->where('id', $get_id);
            $this->db->update('produk');
            $this->session->set_flashdata(
                'message',
                '<div class="alert alert-success alert-dismissible">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                            <h4><i class="icon fa fa-warning"></i> Success!</h4>
                                            Produk berhasil di edit!
                                        </div>'
            );
            redirect('managerutama/produk');
        }
    }
}
