<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pemesanansupplier extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        is_logged_in();
        mu_only();
        $this->load->model('Model_psupplier');
    }

    public function index()
    {
        $data['title'] = "Pemesanan Ke Supplier";
        $data['menu_active'] = "#produk-supplier";
        $data['pemesanan'] = $this->Model_psupplier->getAllData()->result();
        $data['user'] = $this->db->get_where('user', ['username' => $this->session->userdata('username')])->row_array();
        $this->template->load('AdminLTE', 'manajer-utama/pemesanansupplier', $data);
    }

    public function add()
    {
        $data['title'] = "Pemesanan Ke Supplier";
        $data['menu_active'] = "#produk-supplier";
        $data['barang'] = $this->db->get('produk')->result();
        $data['user'] = $this->db->get_where('user', ['username' => $this->session->userdata('username')])->row_array();
        $this->template->load('AdminLTE', 'manajer-utama/pemesanansupplieradd', $data);
    }

    public function addbarang()
    {
        $data = [
            'tanggal' => date("Y-m-d H:i:s"),
            'id_user' => $this->session->userdata('user_id')
        ];

        $this->db->insert('psupplier', $data);
        $insert_id = $this->db->insert_id();

        $json = [
            'insert_id' => $insert_id,
            'msg' => 'success',
            'data' => $this->input->post()
        ];

        $data1 = [
            'id_psupplier' => $insert_id,
            'id_barang' => $this->input->post('id_barang'),
            'jumlah' => $this->input->post('jumlah'),
        ];

        $this->db->insert('detail_psupplier', $data1);

        echo json_encode($json);
    }

    public function addbarangnext()
    {
        $data = [
            'id_psupplier' => $this->input->post('id_psupplier'),
            'id_barang' => $this->input->post('id_barang'),
            'jumlah' => $this->input->post('jumlah'),
        ];

        $this->db->insert('detail_psupplier', $data);

        echo json_encode(['msg' => 'success']);
    }

    public function barang($id_psupplier)
    {
        $data = $this->Model_psupplier->getData($id_psupplier)->result();

        echo json_encode($data);
    }

    public function deletebarang($id_detail)
    {
        $this->db->delete('detail_psupplier', ['id' => $id_detail]);
    }

    public function detail($id)
    {
        $data = [
            'title' => 'Pegawai Gudang',
            'user'  => $this->db->get_where('user', ['username' => $this->session->userdata('username')])->row_array(),
            'menu_active'   => '#produk-supplier',
            'pemesanan' => $this->Model_psupplier->getPemesanan($id)->row(),
            'pemesanan1' => $this->Model_psupplier->getPemesanan($id)->result(),
            'jumlah_barang' => $this->Model_psupplier->getPemesanan($id)->num_rows()
        ];
        $this->template->load('AdminLTE', 'manajer-utama/detailpemesanansupplier', $data);
    }
}
