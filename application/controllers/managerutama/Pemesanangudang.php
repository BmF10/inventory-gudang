<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pemesanangudang extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        is_logged_in();
        mu_only();
        $this->load->model('Model_pgudang');
    }

    public function index()
    {
        $data['title'] = "Pemesanan Ke Gudang";
        $data['menu_active'] = "#produk-gudang";
        $data['pemesanan'] = $this->Model_pgudang->getAllData()->result();
        $data['user'] = $this->db->get_where('user', ['username' => $this->session->userdata('username')])->row_array();
        $this->template->load('AdminLTE', 'manajer-utama/pemesanangudang', $data);
    }

    public function add()
    {
        $data['title'] = "Pemesanan Ke Gudang";
        $data['menu_active'] = "#produk-gudang";
        $data['barang'] = $this->db->get('produk')->result();
        $data['user'] = $this->db->get_where('user', ['username' => $this->session->userdata('username')])->row_array();
        $this->template->load('AdminLTE', 'manajer-utama/pemesanangudangadd', $data);
    }

    public function addbarang()
    {
        $data = [
            'tanggal' => date("Y-m-d H:i:s"),
            'id_user' => $this->session->userdata('user_id'),
            'cabang' => $this->input->post('cabang')
        ];

        $this->db->insert('pgudang', $data);
        $insert_id = $this->db->insert_id();

        $json = [
            'insert_id' => $insert_id,
            'msg' => 'success',
            'data' => $this->input->post()
        ];

        $data1 = [
            'id_pgudang' => $insert_id,
            'id_barang' => $this->input->post('id_barang'),
            'jumlah' => $this->input->post('jumlah'),
        ];

        $this->db->insert('detail_pgudang', $data1);

        echo json_encode($json);
    }

    public function addbarangnext()
    {
        $data = [
            'id_pgudang' => $this->input->post('id_pgudang'),
            'id_barang' => $this->input->post('id_barang'),
            'jumlah' => $this->input->post('jumlah'),
        ];

        $this->db->insert('detail_pgudang', $data);

        echo json_encode(['msg' => 'success']);
    }

    public function barang($id_pgudang)
    {
        $data = $this->Model_pgudang->getData($id_pgudang)->result();

        echo json_encode($data);
    }

    public function deletebarang($id_detail)
    {
        $this->db->delete('detail_pgudang', ['id' => $id_detail]);
    }

    public function detail($id)
    {
        $data = [
            'title' => 'Pegawai Gudang',
            'user'  => $this->db->get_where('user', ['username' => $this->session->userdata('username')])->row_array(),
            'menu_active'   => '#produk-gudang',
            'pemesanan' => $this->Model_pgudang->getPemesanan($id)->row(),
            'pemesanan1' => $this->Model_pgudang->getPemesanan($id)->result(),
            'jumlah_barang' => $this->Model_pgudang->getPemesanan($id)->num_rows()
        ];
        $this->template->load('AdminLTE', 'manajer-utama/detailpemesanan', $data);
    }
}
