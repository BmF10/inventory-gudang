<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        is_logged_in();
        mu_only();
        $this->load->model('Model_akun', 'akun');
    }

    public function index()
    {

        $data['title'] = "List Pegawai";
        $data['menu_active'] = "#user-active";
        $data['user'] = $this->db->get_where('user', ['username' => $this->session->userdata('username')])->row_array();
        $data['akuns'] = $this->akun->getAkun();
        // var_dump($data['akuns']); die;
        $this->template->load('AdminLTE', 'manajer-utama/list_user', $data);
    }

    public function addPegawai($error = NULL)
    {
        $data = [
            'title' => 'Form Input Pegawai Gudang',
            'menu_active'   => '#user-active',
            'jk'    => ['Pria', 'Wanita'],
            'status_nikah'  => ['Sudah', 'Belum'],
            'role'  => [2, 3],
            'user'  => $this->db->get_where('user', ['username' => $this->session->userdata('username')])->row_array(),
            // 'error_foto'     => $error['error']
        ];
        $this->template->load('AdminLTE', 'manajer-utama/add_user', $data);
    }

    public function proses()
    {
        $this->form_validation->set_rules('nama', 'Nama', 'required|trim');
        $this->form_validation->set_rules('jk', 'Jenis Kelamin', 'required|trim');
        $this->form_validation->set_rules('sn', 'Status Nikah', 'required|trim');
        $this->form_validation->set_rules('username', 'Username', 'required|trim|valid_email|is_unique[user.username]', ['is_unique' => 'Username sudah terdaftar!']);
        $this->form_validation->set_rules('pass', 'Password', 'required|trim|min_length[8]');
        $this->form_validation->set_rules('role', 'Role', 'required|trim');

        if ($this->form_validation->run() == FALSE) {
            $this->addPegawai();
        } else {
            $config['upload_path'] = './template/admin/images/profile/';
            $config['allowed_types'] = 'jpg|png|gif|bmp|jpeg';
            $config['max_size'] = '1000000';
            $this->load->library('upload', $config);
            if ( !$this->upload->do_upload('foto') ) {
                // jika validasi file gagal, kirim parameter error ke addPegawai
                $error = ['error' => $this->upload->display_errors()];
                $this->addPegawai($error);
            } else {
                $upload_data = $this->upload->data();
                $foto = $upload_data['file_name'];
                $username = htmlspecialchars($this->input->post('username', true));
                $data = [
                    'nama' => htmlspecialchars($this->input->post('nama', true)),
                    'jk' => $this->input->post('jk', true),
                    'status_nikah' => $this->input->post('sn', true),
                    'username' => $username,
                    'password' => password_hash($this->input->post('pass'), PASSWORD_DEFAULT),
                    'role_id'   =>  $this->input->post('role', true),
                    'foto'  => $foto
                ];

                $this->db->insert('user', $data);
                $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil membuat satu akun!</div>');
                // redirect('ManajerUtama');
                redirect('managerutama/home');
            }
        }
    }

    public function delPegawai($id_pegawai)
    {
        $data = $this->db->get_where('user', ['id' => $id_pegawai])->row_array();
        $foto = $data['foto'];
        
        unlink(FCPATH . 'template/Admin/images/profile/' . $foto);

        $this->db->delete('user', ['id' => $id_pegawai]);
        $this->session->set_flashdata(
            'message',
            '<div class="alert alert-success alert-dismissible">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                            <h4><i class="nav-icon fas fa-warning"></i> Success!</h4>
                                            Deleted pegawai !!!
                                        </div>'
        );
        // redirect('Produk');
        redirect('managerutama/home');
    }

    public function editPegawai($id_pegawai = null)
    {
        $getId = $this->db->get_where('user', ['id' => $id_pegawai])->row_array();
        $foto = $getId['foto'];
        // var_dump($data); die;
        // $this->form_validation->set_rules('nama', 'Nama', 'required|trim');
        // $this->form_validation->set_rules('jk', 'Jenis Kelamin', 'required|trim');
        // $this->form_validation->set_rules('sn', 'Status Nikah', 'required|trim');
        // $this->form_validation->set_rules('username', 'Username', 'required|trim|is_unique[user.username]', ['is_unique' => 'Username sudah terdaftar!']);
        // $this->form_validation->set_rules('pass', 'Password', 'required|trim|min_length[8]');
        $this->form_validation->set_rules('role', 'Role', 'required|trim');
        $data = [
                    'user_id' => $getId,
                    'title' => 'Form Edit Pegawai Gudang',
                    'menu_active'   => '#user-active',
                    'jk'    => ['Pria', 'Wanita'],
                    'status_nikah'  => ['Sudah', 'Belum'],
                    'role'  => [2, 3],
                    // 'error' => $error['error'],
                    'user'  => $this->db->get_where('user', ['username' => $this->session->userdata('username')])->row_array(),
        ];

        if ($this->form_validation->run() == false) {
            $this->template->load('AdminLTE', 'manajer-utama/edit_user', $data);
        } else {
            // $data = [
                    // 'nama' => htmlspecialchars($this->input->post('nama', true)),
                    // 'jk' => $this->input->post('jk', true),
                    // 'status_nikah' => $this->input->post('sn', true),
                    // 'username' => $this->input->post('username', true),
                    // 'password' => password_hash($this->input->post('pass'), PASSWORD_DEFAULT),
                    // 'role_id'   =>  $this->input->post('role', true),
                    // 'foto'  => $foto
                // ];
                $username =  $this->input->post('username', true);
                $role_id =  $this->input->post('role', true);
                $get_id =  $this->input->post('id', true);
                // var_dump($get_id); die;

                // $this->db->set('username', $username);
                $this->db->set('role_id', $role_id);
                $this->db->where('id', $get_id);
                $this->db->update('user');
                $this->session->set_flashdata('message', 
                                        '<div class="alert alert-success alert-dismissible">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                            <h4><i class="icon fa fa-warning"></i> Success!</h4>
                                            Pegawai berhasil di edit!
                                        </div>');
                redirect('ManajerUtama');
        }
    }
}
