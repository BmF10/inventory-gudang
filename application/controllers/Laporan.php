<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Laporan extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Model_laporan', 'laporans');
    }

    public function barang()
    {
        require_once(APPPATH . 'third_party\PHPExcel-1.8\Classes\PHPExcel.php');
        require_once(APPPATH . 'third_party\PHPExcel-1.8\Classes\PHPExcel\Writer\Excel2007.php');

        $dateNow = date('Y-m-d');
        $dateBefore = date('Y-m-d', strtotime("-1 months"));

        $bmasuk = $this->laporans->getBmasuk()->result();

        $obj = new PHPExcel();


        $obj->getProperties()->setCreator('Inventory Gudang 2020');
        $obj->getProperties()->setTitle('Data Barang');
        $obj->getProperties()->setSubject('');
        $obj->getProperties()->setLastModifiedBy('Inventory');
        $obj->getProperties()->setDescription('');

        $obj->setActiveSheetIndex(0);

        //Barang Masuk
        $obj->getActiveSheet()->setCellValue('B2', "Laporan Barang Masuk Per '$dateBefore' sampai '$dateNow' ");
        $obj->getActiveSheet()->setCellValue('B4', 'No');
        $obj->getActiveSheet()->setCellValue('C4', 'Tanggal Input');
        $obj->getActiveSheet()->setCellValue('D4', 'Batch');
        $obj->getActiveSheet()->setCellValue('E4', 'Jenis Item');
        $obj->getActiveSheet()->setCellValue('F4', 'Qty');
        $obj->getActiveSheet()->setCellValue('G4', 'Satuan');
        $obj->getActiveSheet()->setCellValue('H4', 'Penginput');

        $baris = 5;
        $index = 1;
        foreach ($bmasuk as $row) {
            $obj->getActiveSheet()->setCellValue('B' . $baris, $index);
            $obj->getActiveSheet()->setCellValue('C' . $baris, $row->tanggal);
            $obj->getActiveSheet()->setCellValue('D' . $baris, $row->id_bmasuk);
            $obj->getActiveSheet()->setCellValue('E' . $baris, $row->nama);
            $obj->getActiveSheet()->setCellValue('F' . $baris, $row->jumlah);
            $obj->getActiveSheet()->setCellValue('G' . $baris, $row->satuan);
            $obj->getActiveSheet()->setCellValue('H' . $baris, $row->user);

            $baris++;
            $index++;
        }


        //Barang Keluar
        $obj->getActiveSheet()->setCellValue('K2', "Laporan Barang Keluar Per '$dateBefore' sampai '$dateNow' ");
        $obj->getActiveSheet()->setCellValue('K4', 'No');
        $obj->getActiveSheet()->setCellValue('L4', 'Tanggal Input');
        $obj->getActiveSheet()->setCellValue('M4', 'Batch');
        $obj->getActiveSheet()->setCellValue('N4', 'Jenis Item');
        $obj->getActiveSheet()->setCellValue('O4', 'Qty');
        $obj->getActiveSheet()->setCellValue('P4', 'Satuan');
        $obj->getActiveSheet()->setCellValue('Q4', 'Penginput');
        $obj->getActiveSheet()->setCellValue('R4', 'Cabang');

        $bkeluar = $this->laporans->getBkeluar()->result();

        $barisbkeluar = 5;
        $indexbkeluar = 1;
        foreach ($bkeluar as $row) {
            $obj->getActiveSheet()->setCellValue('K' . $barisbkeluar, $indexbkeluar);
            $obj->getActiveSheet()->setCellValue('L' . $barisbkeluar, $row->tanggal);
            $obj->getActiveSheet()->setCellValue('M' . $barisbkeluar, $row->id_bkeluar);
            $obj->getActiveSheet()->setCellValue('N' . $barisbkeluar, $row->nama);
            $obj->getActiveSheet()->setCellValue('O' . $barisbkeluar, $row->jumlah);
            $obj->getActiveSheet()->setCellValue('P' . $barisbkeluar, $row->satuan);
            $obj->getActiveSheet()->setCellValue('Q' . $barisbkeluar, $row->user);
            $obj->getActiveSheet()->setCellValue('R' . $barisbkeluar, $row->cabang);

            $barisbkeluar++;
            $indexbkeluar++;
        }

        //Barang Rusak
        $obj->getActiveSheet()->setCellValue('T2', "Laporan Barang Rusak Per '$dateBefore' sampai '$dateNow' ");
        $obj->getActiveSheet()->setCellValue('T4', 'No');
        $obj->getActiveSheet()->setCellValue('U4', 'Tanggal Input');
        $obj->getActiveSheet()->setCellValue('V4', 'Batch');
        $obj->getActiveSheet()->setCellValue('W4', 'Jenis Item');
        $obj->getActiveSheet()->setCellValue('X4', 'Qty');
        $obj->getActiveSheet()->setCellValue('Y4', 'Satuan');
        $obj->getActiveSheet()->setCellValue('Z4', 'Penginput');
        $obj->getActiveSheet()->setCellValue('AA4', 'Keterangan');

        $brusak = $this->laporans->getBrusak()->result();

        $barisbrusak = 5;
        $indexbrusak = 1;
        foreach ($brusak as $row) {
            $obj->getActiveSheet()->setCellValue('T' . $barisbrusak, $indexbrusak);
            $obj->getActiveSheet()->setCellValue('U' . $barisbrusak, $row->tanggal);
            $obj->getActiveSheet()->setCellValue('V' . $barisbrusak, $row->id_brusak);
            $obj->getActiveSheet()->setCellValue('W' . $barisbrusak, $row->nama);
            $obj->getActiveSheet()->setCellValue('X' . $barisbrusak, $row->jumlah);
            $obj->getActiveSheet()->setCellValue('Y' . $barisbrusak, $row->satuan);
            $obj->getActiveSheet()->setCellValue('Z' . $barisbrusak, $row->user);
            $obj->getActiveSheet()->setCellValue('AA' . $barisbrusak, $row->keterangan);
            $barisbrusak++;
            $indexbrusak++;
        }

        //stok akhir
        $barangAkhir = $this->db->get('produk')->result();

        $obj->getActiveSheet()->setCellValue('AD2', "Laporan Barang Akhir Per '$dateBefore' sampai '$dateNow' ");
        $obj->getActiveSheet()->setCellValue('AD4', 'No');
        $obj->getActiveSheet()->setCellValue('AE4', 'Jenis Item');
        $obj->getActiveSheet()->setCellValue('AF4', 'Qty');
        $obj->getActiveSheet()->setCellValue('AG4', 'Satuan');

        $barisbAkhir = 5;
        $indexbAkhir = 1;
        foreach ($barangAkhir as $row) {
            $obj->getActiveSheet()->setCellValue('AD' . $barisbAkhir, $indexbAkhir);
            $obj->getActiveSheet()->setCellValue('AE' . $barisbAkhir, $row->nama);
            $obj->getActiveSheet()->setCellValue('AF' . $barisbAkhir, $row->jumlah);
            $obj->getActiveSheet()->setCellValue('AG' . $barisbAkhir, $row->satuan);

            $barisbAkhir++;
            $indexbAkhir++;
        }

        $filename = "Data Barang " . date("d-m-Y") . ".xlsx";

        $obj->getActiveSheet()->setTitle('Data Barang');

        header("Content-Type: application/vnd.openxmlformats-officedocument.speadsheetml.sheet");
        header("Content-Disposition: attachment;filename=" . $filename . "");
        header("Cache-Control: max-age=0");

        $writer = PHPExcel_IOFactory::createWriter($obj, 'Excel2007');
        $writer->save('php://output');

        exit;
    }

    public function pemesananGudang()
    {
        require_once(APPPATH . 'third_party\PHPExcel-1.8\Classes\PHPExcel.php');
        require_once(APPPATH . 'third_party\PHPExcel-1.8\Classes\PHPExcel\Writer\Excel2007.php');

        $dateNow = date('Y-m-d');
        $dateBefore = date('Y-m-d', strtotime("-1 months"));

        $pgudang = $this->laporans->getPemesananGudang()->result();

        $obj = new PHPExcel();

        $obj->getProperties()->setCreator('Inventory Gudang 2020');
        $obj->getProperties()->setTitle('Data Pemesanan Ke Gudang');
        $obj->getProperties()->setSubject('');
        $obj->getProperties()->setLastModifiedBy('Inventory');
        $obj->getProperties()->setDescription('');

        $obj->setActiveSheetIndex(0);

        $obj->getActiveSheet()->setCellValue('B2', "Data Pemesanan Ke Gudang Per '$dateBefore' sampai '$dateNow' ");
        $obj->getActiveSheet()->setCellValue('B4', 'No');
        $obj->getActiveSheet()->setCellValue('C4', 'Tanggal Input');
        $obj->getActiveSheet()->setCellValue('D4', 'Batch');
        $obj->getActiveSheet()->setCellValue('E4', 'Jenis Item');
        $obj->getActiveSheet()->setCellValue('F4', 'Qty');
        $obj->getActiveSheet()->setCellValue('G4', 'Satuan');
        $obj->getActiveSheet()->setCellValue('H4', 'Penginput');
        $obj->getActiveSheet()->setCellValue('I4', 'Status');
        $obj->getActiveSheet()->setCellValue('J4', 'Cabang');

        $baris = 5;
        $index = 1;
        foreach ($pgudang as $row) {
            $obj->getActiveSheet()->setCellValue('B' . $baris, $index);
            $obj->getActiveSheet()->setCellValue('C' . $baris, $row->tanggal);
            $obj->getActiveSheet()->setCellValue('D' . $baris, $row->id_pgudang);
            $obj->getActiveSheet()->setCellValue('E' . $baris, $row->produk);
            $obj->getActiveSheet()->setCellValue('F' . $baris, $row->jumlah);
            $obj->getActiveSheet()->setCellValue('G' . $baris, $row->satuan);
            $obj->getActiveSheet()->setCellValue('H' . $baris, $row->user);
            $obj->getActiveSheet()->setCellValue('I' . $baris, $row->is_read == '1' ? 'Sudah Diproses' : 'Belum Diproses');
            $obj->getActiveSheet()->setCellValue('J' . $baris, $row->cabang);


            $baris++;
            $index++;
        }

        $filename = "Data Pemesanan Ke Gudang " . date("d-m-Y") . ".xlsx";

        $obj->getActiveSheet()->setTitle('Data Pemesanan Ke Gudang');

        header("Content-Type: application/vnd.openxmlformats-officedocument.speadsheetml.sheet");
        header("Content-Disposition: attachment;filename=" . $filename . "");
        header("Cache-Control: max-age=0");

        $writer = PHPExcel_IOFactory::createWriter($obj, 'Excel2007');
        $writer->save('php://output');

        exit;
    }

    public function pemesananSupplier()
    {
        require_once(APPPATH . 'third_party\PHPExcel-1.8\Classes\PHPExcel.php');
        require_once(APPPATH . 'third_party\PHPExcel-1.8\Classes\PHPExcel\Writer\Excel2007.php');

        $dateNow = date('Y-m-d');
        $dateBefore = date('Y-m-d', strtotime("-1 months"));

        $psupplier = $this->laporans->getPemesananSupplier()->result();

        $obj = new PHPExcel();

        $obj->getProperties()->setCreator('Inventory Gudang 2020');
        $obj->getProperties()->setTitle('Data Pemesanan Ke Supplier');
        $obj->getProperties()->setSubject('');
        $obj->getProperties()->setLastModifiedBy('Inventory');
        $obj->getProperties()->setDescription('');

        $obj->setActiveSheetIndex(0);

        $obj->getActiveSheet()->setCellValue('B2', "Data Pemesanan Ke Supplier Per '$dateBefore' sampai '$dateNow' ");
        $obj->getActiveSheet()->setCellValue('B4', 'No');
        $obj->getActiveSheet()->setCellValue('C4', 'Tanggal Input');
        $obj->getActiveSheet()->setCellValue('D4', 'Batch');
        $obj->getActiveSheet()->setCellValue('E4', 'Jenis Item');
        $obj->getActiveSheet()->setCellValue('F4', 'Qty');
        $obj->getActiveSheet()->setCellValue('G4', 'Satuan');
        $obj->getActiveSheet()->setCellValue('H4', 'Penginput');

        $baris = 5;
        $index = 1;
        foreach ($psupplier as $row) {
            $obj->getActiveSheet()->setCellValue('B' . $baris, $index);
            $obj->getActiveSheet()->setCellValue('C' . $baris, $row->tanggal);
            $obj->getActiveSheet()->setCellValue('D' . $baris, $row->id_psupplier);
            $obj->getActiveSheet()->setCellValue('E' . $baris, $row->produk);
            $obj->getActiveSheet()->setCellValue('F' . $baris, $row->jumlah);
            $obj->getActiveSheet()->setCellValue('G' . $baris, $row->satuan);
            $obj->getActiveSheet()->setCellValue('H' . $baris, $row->user);

            $baris++;
            $index++;
        }

        $filename = "Data Pemesanan Ke Supplier " . date("d-m-Y") . ".xlsx";

        $obj->getActiveSheet()->setTitle('Data Pemesanan Ke Supplier');

        header("Content-Type: application/vnd.openxmlformats-officedocument.speadsheetml.sheet");
        header("Content-Disposition: attachment;filename=" . $filename . "");
        header("Cache-Control: max-age=0");

        $writer = PHPExcel_IOFactory::createWriter($obj, 'Excel2007');
        $writer->save('php://output');

        exit;
    }
}
