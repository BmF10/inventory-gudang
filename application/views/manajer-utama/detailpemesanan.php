<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Pemesanan Dari Manager Ke Gudang</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item"><a href="#">Pemensanan Barang Ke Gudang</a></li>
                    <li class="breadcrumb-item active">Detail</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <!-- general form elements disabled -->
                <div class="card card-default">
                    <div class="card-header">
                        <h3 class="card-title">Pemesanan Dari Manager Ke Gudang</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-4">
                                <h3>Pemesanan</h3>
                                <table>
                                    <tr>
                                        <td>Tanggal&nbsp;&nbsp; </td>
                                        <td>:&nbsp;&nbsp;</td>
                                        <td><?= $pemesanan->tanggal ?></td>
                                    </tr>
                                    <tr>
                                        <td>Nama Penginput&nbsp;&nbsp;</td>
                                        <td>:&nbsp;&nbsp;</td>
                                        <td><?= $pemesanan->nama_user ?></td>
                                    </tr>
                                    <tr>
                                        <td>Cabang&nbsp;&nbsp;</td>
                                        <td>:&nbsp;&nbsp;</td>
                                        <td><?= $pemesanan->cabang ?></td>
                                    </tr>
                                    <tr>
                                        <td>Jumlah Barang&nbsp;&nbsp;</td>
                                        <td>:&nbsp;&nbsp;</td>
                                        <td><?= $jumlah_barang ?></td>
                                    </tr>
                                    <tr>
                                        <td>Status&nbsp;&nbsp;</td>
                                        <td>:&nbsp;&nbsp;</td>
                                        <td><?= $pemesanan->is_read == '0' ? 'Waiting' : 'Finish' ?></td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-sm-8">
                                <h3>Data Barang</h3>
                                <table id="table" class="table table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Nama</th>
                                            <th scope="col">Jumlah</th>
                                            <th scope="col">Satuan</th>
                                            <th scope="col">Stok Tersedia</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no = 1;
                                        foreach ($pemesanan1 as $p) : ?>
                                            <tr>
                                                <td><?= $no++;  ?></td>
                                                <td><?= $p->nama_barang; ?></td>
                                                <td><?= $p->permintaan; ?></td>
                                                <td><?= $p->satuan; ?></td>
                                                <td><?= $p->stok; ?></td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <a href="<?= base_url('managerutama/pemesanangudang') ?>" type="button" class="btn btn-primary float-right" value="save" id="save">Keluar</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>