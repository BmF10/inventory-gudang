<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">List Pegawai</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <!-- <li class="breadcrumb-item"><a href="#">Home</a></li> -->
                    <li class="breadcrumb-item active">List Pegawai</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <?= $this->session->flashdata('message') ?>
        <a href="<?= base_url('laporan/barang') ?>" class="btn btn-primary">Laporan Barang</a>
        <a href="<?= base_url('laporan/pemesananGudang') ?>" class="btn btn-primary">Laporan Pemesanan ke Gudang</a>
        <a href="<?= base_url('laporan/pemesananSupplier') ?>" class="btn btn-primary">Laporan Pemesanan ke Supplier</a>
        <br><br>
        <div class="card">
            <div class="card-header">
                <!-- <h3 class="card-title">DataTable with default features</h3> -->
                <a href="<?= base_url('managerutama/home/addPegawai') ?>" class="btn btn-primary"><i class="fa fa-plus"></i> Add Pegawai</a>
                <!-- <a href="<?= base_url('ManajerUtama/addPegawai') ?>" class="btn btn-primary"><i class="fa fa-plus"></i> Add Pegawai</a>
 -->
            </div>
            <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>Jenis Kelamin</th>
                            <th>Status Menikah</th>
                            <th>Foto</th>
                            <th>username</th>
                            <th>Role</th>
                            <th>action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no = 1;
                        foreach ($akuns as $key => $akun) : ?>
                            <tr>
                                <td><?= $no++;  ?></td>
                                <td><?= $akun['nama']; ?></td>
                                <td><?= $akun['jk']; ?></td>
                                <td><?= $akun['status_nikah']; ?></td>
                                <td><img src="<?= base_url('template/admin/') ?>images/profile/<?= $akun['foto']; ?>" height="50" width="50"></td>
                                <td><?= $akun['username']; ?></td>
                                <td>
                                    <?php if ($akun['role_id'] == 1) : ?>
                                        Manajer Utama
                                    <?php elseif ($akun['role_id'] == 2) : ?>
                                        <p style="color: blue; font-weight: bold;">Manajer Gudang</p>
                                    <?php else : ?>
                                        <p style="color: green; font-weight: bold;">Pegawai Gudang</p>
                                    <?php endif; ?>
                                </td>
                                <td>
                                    <a href="<?= base_url('managerutama/home/editPegawai/') . $akun['id'] ?>" class="btn btn-success">
                                        <!-- <i class="fas fa-arrow-left"></i> -->
                                        Edit
                                    </a>
                                    <a href="#" data-toggle="modal" data-target="#modal-hapus-<?= $akun['id'] ?>" class="btn btn-danger">
                                        Hapus
                                    </a>
                                </td>
                            </tr>

                            <div class="modal fade" id="modal-hapus-<?= $akun['id'] ?>">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title">Delete Akun Pegawai</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <h2>Ingin menghapus "<?= $akun['username'] ?>" akun dari "<?= $akun['nama'] ?>" dari list pegawai ?</h2>
                                        </div>
                                        <div class="modal-footer justify-content-between">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <a href="<?= base_url('managerutama/home/delPegawai/') . $akun['id'] ?>" class="btn btn-primary"> Ya</a>
                                        </div>
                                        <!-- </form> -->
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                        <?php endforeach; ?>
                    </tbody>
                    <tfoot>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</section>