<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Add Pegawai</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item"><a href="<?= base_url('ManajerUtama') ?>">List Pegawai</a></li>
                    <li class="breadcrumb-item active">Edit Pegawai</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <!-- general form elements disabled -->
                <div class="card card-warning">
                    <div class="card-header">
                        <!-- <h3 class="card-title">Form Add Pegawai Gudang</h3> -->
                        <h3 class="card-title"><a href="<?= base_url('managerutama/home') ?>"><i class="fas fa-arrow-left"></i></a></h3>
                    </div>
                    <form role="form" action="<?= base_url('ManajerUtama/editPegawai') ?>" method="POST" enctype="multipart/form-data">
                        <input type="hidden" name="id" value="<?= $user_id['id'];  ?>">
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <!-- text input -->
                                    <div class="form-group">
                                        <label>Nama</label>
                                        <input type="text" class="form-control <?= form_error('nama') ? 'is-invalid' : null ?>" placeholder="Enter ..." name="nama" value="<?= $user_id['nama'];  ?>" disabled>
                                        <small style="color:red"><?= form_error('nama') ?></small>
                                    </div>
                                    <div class="form-group">
                                        <label>Status Menikah</label>
                                        <select name="sn" id="sn" class="form-control <?= form_error('sn') ? 'is-invalid' : null ?>" disabled>
                                            <option value="" disabled selected>- Pilih -</option>
                                            <?php foreach ($status_nikah as $sn) : ?>
                                                <?php if ($sn == $user_id['status_nikah']) : ?>
                                                    <option value="<?= $user_id['status_nikah'];  ?>" selected><?= $sn;  ?></option>
                                                <?php else : ?>
                                                    <option value="<?= $sn ?>"><?= $sn ?></option>
                                                <?php endif; ?>
                                            <?php endforeach ?>
                                        </select>
                                        <small style="color:red"><?= form_error('sn') ?></small>
                                    </div>
                                    <div class="form-group">
                                        <label>Jenis Kelamin</label>
                                        <?php foreach ($jk as $j) : ?>
                                            <?php if ($j == $user_id['jk']) : ?>
                                                <div class="custom-control custom-radio">
                                                    <input class="custom-control-input text-primary" type="radio" id="<?= $j; ?>" name="jk" value="<?= $user_id['jk']; ?>" checked disabled>
                                                    <label for="<?= $j; ?>" class="custom-control-label"><?= $j; ?></label>
                                                </div>
                                            <?php else : ?>
                                                <div class="custom-control custom-radio">
                                                    <input class="custom-control-input" type="radio" id="<?= $j; ?>" name="jk" value="<?= $j; ?>" disabled>
                                                    <label for="<?= $j; ?>" class="custom-control-label"><?= $j; ?></label>
                                                </div>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                        <small style="color:red"><?= form_error('jk') ?></small>
                                    </div>
                                    <?php
                                    // if ($error) {
                                    //     $er = "is-invalid";
                                    // } else {
                                    //     $er = null;
                                    // }
                                    ?>
                                    <div class="form-group">
                                        <label for="exampleInputFile">Foto</label>
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <img style="border-radius: 5px" class="profile-user-img img-responsive" src="<?= base_url('template/admin/') ?>images/profile/<?= $user_id['foto']; ?>" alt="User profile picture">
                                                </div>
                                                <div class="input-group col-sm-9">
                                                    <!-- <div class="custom-file">
                                                        <input type="file" class="custom-file-input" id=" exampleInputFile" name="foto">
                                                        <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                                                    </div> -->
                                                    <!-- <div class="input-group-append">
                                                            <span class="input-group-text" id="">Upload</span>
                                                        </div> -->
                                                </div>
                                            </div>
                                        <small style="color:red"></small>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Username</label>
                                        <input type="text" class="form-control <?= form_error('username') ? 'is-invalid' : null ?>" placeholder="Enter ..." name="username" value="<?= $user_id['username'];  ?>" disabled>
                                        <small style="color:red"><?= form_error('username') ?></small>
                                    </div>
                                    <!-- <div class="form-group">
                                        <label>Password</label>
                                        <input type="password" class="form-control <?= form_error('pass') ? 'is-invalid' : null ?>" placeholder="Enter ..." name="pass">
                                        <small style="color:red"><?= form_error('pass') ?></small>
                                    </div> -->
                                    <div class="form-group">
                                        <label>Role</label>
                                        <select name="role" id="role" class="form-control <?= form_error('role') ? 'is-invalid' : null ?>">
                                            <option value="" disabled selected>- Pilih -</option>
                                            <?php foreach ($role as $r) : ?>
                                                <?php if ($r == $user_id['role_id']) : ?>
                                                    <option value="<?= $user_id['role_id'] ?>" selected>
                                                        <?php if ($r == 2) : ?>
                                                            Manajer Gudang
                                                        <?php else : ?>
                                                            Pegawai Gudang
                                                        <?php endif; ?>
                                                    </option>
                                                <?php else : ?>
                                                    <option value="<?= $r ?>">
                                                        <?php if ($r == 2) : ?>
                                                            Manajer Gudang
                                                        <?php else : ?>
                                                            Pegawai Gudang
                                                        <?php endif; ?>
                                                    </option>
                                                <?php endif; ?>
                                            <?php endforeach ?>
                                        </select>
                                        <small style="color:red"><?= form_error('role') ?></small>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary float-right">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<!-- /.content -->
<script>
    $(function() {
        $('.select2').select2()
    })
</script>