<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Pemensanan Barang Ke Supplier</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Pemensanan Barang Ke Supplier</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <a class="btn btn-primary" href="<?= base_url('/managerutama/pemesanansupplier/add') ?>">
            <i class="fas fa-plus"></i>
            Tambah Pemesanan
        </a><br><br>
        <div class="card">
            <div class="card-header">
                <h3>Data Pemesanan</h3>
            </div>
            <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Tanggal</th>
                            <th>Nama Pemesan</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no = 1;
                        foreach ($pemesanan as $p) : ?>
                            <tr>
                                <td><?= $no++;  ?></td>
                                <td><?= $p->tanggal; ?></td>
                                <td><?= $p->nama; ?></td>
                                <td><a href="<?= base_url('managerutama/pemesanansupplier/detail/') . $p->id ?>" class="btn btn-primary">Detail</a></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                    <tfoot>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</section>