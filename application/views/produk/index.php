<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark">List Produk</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item active">List Produk</li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <?= $this->session->flashdata('message') ?>
    <div class="card">
      <div class="card-header">
        <!-- <h3 class="card-title">DataTable with default features</h3> -->
        <!-- <a href="<?= base_url('produk/addProduk') ?>" class="btn btn-primary"><i class="fa fa-plus"></i> Add Produk</a> -->
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-default">
          <i class="fa fa-plus"></i> Add Produk
        </button>
      </div>
      <div class="card-body">
        <table id="example1" class="table table-bordered table-striped">
          <thead>
            <tr>
              <th>No</th>
              <th>Nama Barang</th>
              <th>Satuan</th>
              <th>Stock Tersedia</th>
              <th style="text-align: center;">Action</th>
            </tr>
          </thead>
          <tbody>
            <?php $no = 1; ?>
            <?php foreach ($produks as $key => $produk) : ?>
              <tr>
                <td><?= $no++;  ?></td>
                <td><?= $produk['nama'];  ?></td>
                <td><?= $produk['satuan'];  ?></td>
                <td><?= $produk['jumlah'];  ?></td>
                <td align="center">
                  <a href="<?= base_url('managerutama/Produk/editProduk/') . $produk['id'] ?>" class="btn btn-success"> Edit</a>
                  <a href="#" data-toggle="modal" data-target="#modal-hapus-<?= $produk['id'] ?>" class="btn btn-danger"> Hapus</a>
                </td>
              </tr>
              <div class="modal fade" id="modal-hapus-<?= $produk['id'] ?>">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title">Delete Produk</h4>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <h2>Ingin menghapus "<?= $produk['nama'] ?>" dari list produk ?</h2>
                    </div>
                    <div class="modal-footer justify-content-between">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                      <a href="<?= base_url('managerutama/Produk/delProduk/') . $produk['id'] ?>" class="btn btn-primary"> Ya</a>
                    </div>
                    </form>
                  </div>
                  <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
              </div>
            <?php endforeach; ?>
          </tbody>
          <!-- <tfoot>
                        <tr>
                            <th>No</th>
                            <th>Nama Barang</th>
                            <th>Jumlah</th>
                            <th style="text-align: center;">Action</th>
                        </tr>
                    </tfoot> -->
        </table>
      </div>
    </div>
  </div>
  <div class="modal fade" id="modal-default">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Add Produk</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form role="form" method="post" action="<?= base_url('managerutama/Produk/addProduk'); ?>">
            <div class="form-group">
              <label for="exampleInputEmail1">Nama Barang</label>
              <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter nama barang" name="nama" required="">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail2">Satuan Barang</label>
              <input type="text" class="form-control" id="exampleInputEmail2" placeholder="Enter satuan barang" name="satuan" required="">
            </div>
            <!-- </form> -->
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save</button>
        </div>
        </form>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->
</section>