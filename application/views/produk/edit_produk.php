<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Add Pegawai</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item"><a href="<?= base_url('managerutama/produk') ?>">List Produk</a></li>
                    <li class="breadcrumb-item active">Edit Pegawai</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <!-- general form elements disabled -->
                <div class="card card-warning">
                    <div class="card-header">
                        <!-- <h3 class="card-title">Form Add Pegawai Gudang</h3> -->
                        <h3 class="card-title"><a href="<?= base_url('managerutama/produk') ?>"><i class="fas fa-arrow-left"></i></a></h3>
                    </div>
                    <form role="form" action="<?= base_url('managerutama/produk/editProduk') ?>" method="POST">
                        <input type="hidden" name="id" value="<?= $produk_id['id'];  ?>">
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <!-- text input -->
                                    <div class="form-group">
                                        <label>Nama Barang</label>
                                        <input type="text" class="form-control <?= form_error('nama') ? 'is-invalid' : null ?>" placeholder="Enter ..." name="nama" value="<?= $produk_id['nama'];  ?>">
                                        <small style="color:red"><?= form_error('nama') ?></small>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Satuan</label>
                                        <input type="text" class="form-control <?= form_error('satuan') ? 'is-invalid' : null ?>" placeholder="Enter ..." name="satuan" value="<?= $produk_id['satuan'];  ?>">
                                        <small style="color:red"><?= form_error('satuan') ?></small>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary float-right">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<!-- /.content -->
<script>
    $(function() {
        $('.select2').select2()
    })
</script>