<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?= $title; ?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?= base_url('template/admin/') ?>plugins/fontawesome-free/css/all.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Tempusdominus Bbootstrap 4 -->
    <link rel="stylesheet" href="<?= base_url('template/admin/') ?>plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?= base_url('template/admin/') ?>plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <!-- JQVMap -->
    <link rel="stylesheet" href="<?= base_url('template/admin/') ?>plugins/jqvmap/jqvmap.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?= base_url('template/admin/') ?>dist/css/adminlte.min.css">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="<?= base_url('template/admin/') ?>plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="<?= base_url('template/admin/') ?>plugins/daterangepicker/daterangepicker.css">
    <!-- summernote -->
    <link rel="stylesheet" href="<?= base_url('template/admin/') ?>plugins/summernote/summernote-bs4.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <!-- DataTables -->
    <link rel="stylesheet" href="<?= base_url('template/admin/') ?>plugins/datatables-bs4/css/dataTables.bootstrap4.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="<?= base_url('template/admin/') ?>plugins/select2/css/select2.min.css">
    <link rel="stylesheet" href="<?= base_url('template/admin/') ?>plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
</head>


<!-- jQuery -->
<script src="<?= base_url('template/admin/') ?>plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?= base_url('template/admin/') ?>plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Select2 -->
<script src="<?= base_url('template/admin/') ?>plugins/select2/js/select2.full.min.js"></script>

<body class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">

        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand navbar-white navbar-light">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
                </li>
                <!-- <li class="nav-item d-none d-sm-inline-block">
                    <a href="index3.html" class="nav-link">Home</a>
                </li> -->
            </ul>

            <!-- SEARCH FORM -->
            <!-- form class="form-inline ml-3">
                <div class="input-group input-group-sm">
                    <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
                    <div class="input-group-append">
                        <button class="btn btn-navbar" type="submit">
                            <i class="fas fa-search"></i>
                        </button>
                    </div>
                </div>
            </form> -->

            <!-- Right navbar links -->
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#">
                        <i class="fas fa-th-large"></i>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-dark-primary elevation-4">
            <!-- Brand Logo -->
            <a href="index3.html" class="brand-link">
                <!--   <img src="<?= base_url('template/admin/') ?>dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8"> -->
                <img src="<?= base_url('template/admin/images/logo.svg'); ?>" height="50" width="50" style="opacity: .8">
                <span class="brand-text font-weight-light">A&W</span>
            </a>

            <!-- Sidebar -->
            <div class="sidebar">
                <!-- Sidebar user panel (optional) -->
                <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                    <div class="image">
                        <img src="<?= base_url('template/admin/') ?>images/profile/<?= $user['foto']; ?>" class="img-circle elevation-2" alt="User Image">
                    </div>
                    <div class="info">
                        <a href="#" class="d-block"><?= $user['nama']; ?></a>
                    </div>
                </div>

                <!-- Sidebar Menu -->
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                        <!-- Add icons to the links using the .nav-icon class
                            with font-awesome or any other icon font library -->
                        <li class="nav-header">MENU</li>
                        <!-- Menu Manajer Utama -->
                        <?php if ($user['role_id'] == 1) : ?>
                            <li class="nav-item">
                                <a href="<?= base_url('managerutama/home') ?>" class="nav-link" id="user-active">
                                    <i class="nav-icon fas fa-users text-warning"></i>
                                    <p class="text">User</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= base_url('managerutama/produk') ?>" class="nav-link" id="produk-active">
                                    <i class="nav-icon fas fa-tags text-info"></i>
                                    <p class="text">Produk</p>
                                </a>
                            </li>
                            <li class="nav-item has-treeview">
                                <!-- menu-open -->
                                <a href="#" class="nav-link">
                                    <!-- active -->
                                    <i class="nav-icon fas fa-edit text-success"></i>
                                    <p>
                                        Pemesanan
                                        <i class="right fas fa-angle-left"></i>
                                    </p>
                                </a>
                                <ul class="nav nav-treeview" style="display: none;">
                                    <!-- block -->
                                    <li class="nav-item">
                                        <a href="<?= base_url('managerutama/pemesanansupplier') ?>" id="produk-supplier" class="nav-link">
                                            <!-- active -->
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Produk ke Supplier</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="<?= base_url('managerutama/pemesanangudang') ?>" id="produk-gudang" class="nav-link">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Produk ke Gudang</p>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <!-- Menu Manajer Gudang -->
                        <?php elseif ($user['role_id'] == 2) : ?>
                            <li class="nav-item">
                                <a href="<?= base_url('managergudang/home') ?>" class="nav-link" id="menu-manajer-gudang-home">
                                    <i class="nav-icon fas fa-home text-success"></i>
                                    <p class="text">Home</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= base_url('managergudang/evaluasi') ?>" class="nav-link" id="menu-manajer-gudang-evaluasi">
                                    <i class="nav-icon fas fa-clipboard text-warning"></i>
                                    <p class="text">Evaluasi Gudang</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= base_url('managergudang/retur') ?>" class="nav-link" id="retur">
                                    <i class="nav-icon fas fa-people-carry text-danger"></i>
                                    <p class="text">Retur Barang</p>
                                </a>
                            </li>
                            <!-- Menu Pegawai Gudang -->
                        <?php else : ?>
                            <li class="nav-item">
                                <a href="<?= base_url('pegawaigudang/home') ?>" class="nav-link" id="menu-pegawai">
                                    <i class="nav-icon fas fa-home"></i>
                                    <p>Home</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= base_url('pegawaigudang/pemesanangudang') ?>" class="nav-link" id="menu-pegawai-pemesanan">
                                    <i class="nav-icon fas fa-file-invoice"></i>
                                    <p>Data Pemesanan</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= base_url('pegawaigudang/produk') ?>" class="nav-link" id="menu-data-produk">
                                    <i class="nav-icon fas fa-box text-default"></i>
                                    <p>Data Produk</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= base_url('pegawaigudang/barangmasuk') ?>" class="nav-link" id="menu-pegawai-masuk">
                                    <i class="nav-icon fas fa-box-open text-success"></i>
                                    <p>Data Produk Masuk</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= base_url('pegawaigudang/barangkeluar') ?>" class="nav-link" id="menu-pegawai-keluar">
                                    <i class="nav-icon fas fa-box-open text-warning"></i>
                                    <p>Data Produk Keluar</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= base_url('pegawaigudang/barangrusak') ?>" class="nav-link" id="menu-pegawai-rusak">
                                    <i class="nav-icon fas fa-box-open text-danger"></i>
                                    <p>Data Produk Rusak</p>
                                </a>
                            </li>
                        <?php endif; ?>

                        <li class="nav-header" align="center"><i class="fa fa-minus-circle"></i></li>

                        <li class="nav-item">
                            <a href="<?= base_url('auth/logout');  ?>" class="nav-link">
                                <i class="nav-icon fas fa-sign-out-alt text-danger"></i>
                                <p>Logout</p>
                            </a>
                        </li>
                    </ul>
                </nav>
                <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <?= $contents ?>
        </div>
        <!-- /.content-wrapper -->
        <footer class="main-footer">
            <strong>Copyright &copy; 2014-2019 <a href="#">AW</a>.</strong>
            All rights reserved.
            <div class="float-right d-none d-sm-inline-block">
                <b>Version</b> 3.0.1
            </div>
        </footer>

        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Control sidebar content goes here -->
        </aside>
        <!-- /.control-sidebar -->
    </div>
    <!-- ./wrapper -->

    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
        $.widget.bridge('uibutton', $.ui.button)
    </script>
    <!-- Bootstrap 4 -->
    <script src="<?= base_url('template/admin/') ?>plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- ChartJS -->
    <script src="<?= base_url('template/admin/') ?>plugins/chart.js/Chart.min.js"></script>
    <!-- Sparkline -->
    <script src="<?= base_url('template/admin/') ?>plugins/sparklines/sparkline.js"></script>
    <!-- JQVMap -->
    <script src="<?= base_url('template/admin/') ?>plugins/jqvmap/jquery.vmap.min.js"></script>
    <script src="<?= base_url('template/admin/') ?>plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
    <!-- jQuery Knob Chart -->
    <script src="<?= base_url('template/admin/') ?>plugins/jquery-knob/jquery.knob.min.js"></script>
    <!-- daterangepicker -->
    <script src="<?= base_url('template/admin/') ?>plugins/moment/moment.min.js"></script>
    <script src="<?= base_url('template/admin/') ?>plugins/daterangepicker/daterangepicker.js"></script>
    <!-- Tempusdominus Bootstrap 4 -->
    <script src="<?= base_url('template/admin/') ?>plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
    <!-- Summernote -->
    <script src="<?= base_url('template/admin/') ?>plugins/summernote/summernote-bs4.min.js"></script>
    <!-- overlayScrollbars -->
    <script src="<?= base_url('template/admin/') ?>plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?= base_url('template/admin/') ?>dist/js/adminlte.js"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="<?= base_url('template/admin/') ?>dist/js/pages/dashboard.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?= base_url('template/admin/') ?>dist/js/demo.js"></script>
    <!-- DataTables -->
    <script src="<?= base_url('template/admin/') ?>plugins/datatables/jquery.dataTables.js"></script>
    <script src="<?= base_url('template/admin/') ?>plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
    <script>
        $(function() {
            $("#example1").DataTable();
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false,
            });
        });
    </script>
    <script>
        $('<?= $menu_active; ?>').addClass('active'); //ambil-di-controller
    </script>
    <script type="text/javascript">
        $('.custom-file-input').on('change', function() {
            let fileName = $(this).val().split('\\').pop();
            $(this).next('.custom-file-label').addClass("selected").html(fileName);
        });
    </script>
</body>

</html>