<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Evaluasi Dari Manager</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active"><a href="#">Evaluasi Dari Manager</a></li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <!-- general form elements disabled -->
                <div class="card card-default">
                    <div class="card-header">
                        <h3 class="card-title">Evaluasi Dari Manager</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-4">
                                <h3>Detail Evaluasi</h3>
                                <img src="<?= base_url('template/admin/') ?>images/profile/<?= $evaluasi->foto; ?>" class="img-rounded elevation-2" /><br><br>
                                <table>
                                    <tr>
                                        <td>Nama Manager&nbsp;&nbsp;</td>
                                        <td>:&nbsp;&nbsp;</td>
                                        <td><?= $evaluasi->nama ?></td>
                                    </tr>
                                    <tr>
                                        <td>Tanggal&nbsp;&nbsp; </td>
                                        <td>:&nbsp;&nbsp;</td>
                                        <td><?= $evaluasi->tanggal ?></td>
                                    </tr>
                                    <tr>
                                        <td>Status&nbsp;&nbsp;</td>
                                        <td>:&nbsp;&nbsp;</td>
                                        <td><?= $evaluasi->is_read == '0' ? 'Belum Dibaca' : 'Sudah Dibaca' ?></td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-sm-8">
                                <h3>Penjelasan</h3>
                                <div class="form-group">
                                    <textarea class="form-control" disabled id="exampleFormControlTextarea1" rows="5"><?= $evaluasi->penjelasan ?></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <?php if ($evaluasi->is_read == '0') : ?>
                            <a href="<?= base_url('pegawaigudang/home/readevaluasi/') . $evaluasi->evaluasi_id ?>" class="btn btn-success">Sudah Dibaca</a>
                        <?php endif ?>
                        <a href="<?= base_url('pegawaigudang/home/') ?>" type="button" class="btn btn-primary float-right" value="save" id="save">Keluar</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>