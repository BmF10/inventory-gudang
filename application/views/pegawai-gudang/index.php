<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Home</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <!-- <li class="breadcrumb-item"><a href="#">Home</a></li> -->
                    <li class="breadcrumb-item active">Home</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <?= $this->session->flashdata('message') ?>
        <a href="<?= base_url('laporan/barang') ?>" class="btn btn-primary">Laporan Barang</a>
        <br><br>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Pemesanan dari Manager yang harus di proses</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body table-responsive p-0" style="height: 300px;">
                        <table class="table table-head-fixed">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Manager</th>
                                    <th>Tanggal</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if ($pemesanan == null) : ?>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>
                                            <h3>Belum Ada Data</h3>
                                        </td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                <?php else : ?>
                                    <?php $no = 1;
                                    foreach ($pemesanan as $p) :
                                    ?>
                                        <tr>
                                            <td><?= $no++ ?></td>
                                            <td><?= $p->nama ?></td>
                                            <td><?= $p->tanggal ?></td>
                                            <td><?= $p->is_read == '0' ? "Waiting" : 'Finish' ?></td>
                                            <td><a href="<?= base_url('pegawaigudang/home/detail/') . $p->id_pgudang ?>" class="btn btn-info">Detail</a></td>
                                        </tr>
                                    <?php endforeach ?>
                                <?php endif ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Evaluasi Dari Manager Gudang</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body table-responsive p-0" style="height: 300px;">
                        <table class="table table-head-fixed">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Manager</th>
                                    <th>Tanggal</th>
                                    <th>Penjelasan</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if ($evaluasi == null) : ?>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>
                                            <h3>Belum Ada Data</h3>
                                        </td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                <?php else : ?>
                                    <?php $no = 1;
                                    foreach ($evaluasi as $p) :
                                    ?>
                                        <tr>
                                            <td><?= $no++ ?></td>
                                            <td><?= $p->nama ?></td>
                                            <td><?= $p->tanggal ?></td>
                                            <td><?= substr($p->penjelasan, 0, 40); ?>...</td>
                                            <td><?= $p->is_read == '0' ? "Belum Dibaca" : 'Sudah Dibaca' ?></td>
                                            <td><a href="<?= base_url('pegawaigudang/home/detailevaluasi/') . $p->id ?>" class="btn btn-info">Detail</a></td>
                                        </tr>
                                    <?php endforeach ?>
                                <?php endif ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
    </div>
</section>