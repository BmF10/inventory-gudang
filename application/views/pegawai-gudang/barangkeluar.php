<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Barang Keluar</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?= base_url('pegawaigudang/home'); ?>">Home</a></li>
                    <li class="breadcrumb-item active">Barang Keluar</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <a class="btn btn-primary" href="<?= base_url('/pegawaigudang/barangkeluar/add') ?>">
            <i class="fa fa-plus"></i> 
            Tambah Barang Keluar
        </a>
        <!-- <h1 style="text-align: center;" class="mt-5">
            <a class="btn btn-default" href="<?= base_url('/pegawaigudang/barangkeluar/add') ?>" title="Tambah Barang Keluar"  style="border:dashed; border-radius: 11px;  color: grey; ">
                    <i class="fas fa-plus-circle fa-10x" style="text-align: center;  padding:22px;"></i>
            </a>
        </h1> --><br><br>
        <div class="card">
            <div class="card-header">
                <h3>Data Produk</h3>
            </div>
            <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Tanggal</th>
                            <th>Nama Penginput</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no = 1;
                        foreach ($bkeluar as $m) : ?>
                            <tr>
                                <td><?= $no++;  ?></td>
                                <td><?= $m->tanggal; ?></td>
                                <td><?= $m->nama; ?></td>
                                <td><a href="<?= base_url('pegawaigudang/barangkeluar/detail/') . $m->id ?>" class="btn btn-primary">Detail</a></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                    <tfoot>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</section>