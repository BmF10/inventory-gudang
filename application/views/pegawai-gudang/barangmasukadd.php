<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Barang Masuk</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?= base_url('pegawaigudang/home'); ?>">Home</a></li>
                    <li class="breadcrumb-item"><a href="<?= base_url('pegawaigudang/barangmasuk'); ?>">Barang Masuk</a></li>
                    <li class="breadcrumb-item active">Tambah</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <!-- general form elements disabled -->
                <div class="card card-default">
                    <div class="card-header">
                        <!-- <h3 class="card-title">Form Add Barang Masuk</h3> -->
                        <h3 class="card-title"><a href="<?= base_url('pegawaigudang/barangmasuk'); ?>"><i class="fa fa-arrow-left"></i></a></h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-5">
                                <h3>Add Barang Masuk</h3>
                                <form id="form_Add">
                                    <input type="hidden" id="id_bmasuk">
                                    <div class="form-group">
                                        <label>Barang</label>
                                        <select class="form-control select2" id="id_barang" style="width: 100%;">
                                            <?php foreach ($barang as $b) : ?>
                                                <option value="<?= $b->id ?>"><?= $b->nama ?></option>
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Jumlah</label>
                                        <input type="number" value="1" min="1" name="jumlah" id="jumlah" class="form-control" placeholder="Jumlah Barang">
                                    </div>
                                    <button type="submit" class="btn btn-primary float-right" value="save" id="save">Simpan</button>
                                </form>
                            </div>
                            <div class="col-sm-1"></div>
                            <div class="col-sm-6">
                                <h3>Data Barang</h3>
                                <br>
                                <table id="table" class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Nama</th>
                                            <th scope="col">Jumlah</th>
                                            <th scope="col">Satuan</th>
                                            <th scope="col">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <a href="<?= base_url('pegawaigudang/barangmasuk') ?>" type="button" class="btn btn-success float-right" value="save" id="save">Selesai</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    $(function() {
        $('.select2').select2({
            theme: 'bootstrap4'
        })

        Delete = (id) => {
            $.ajax({
                url: "<?= base_url('pegawaigudang/barangmasuk/deletebarang/') ?>" + id,
                success: () => {
                    $('#table tbody tr').remove();
                    table()
                }
            })
        }

        table = () => {
            id_bmasuk = $('#id_bmasuk').val();
            $.ajax({
                url: "<?= base_url('pegawaigudang/barangmasuk/barang/') ?>" + id_bmasuk,
                dataType: 'json',
                success: (data) => {
                    console.log(data);
                    for (let i = 0; i < data.length; i++) {
                        no = i + 1;

                        html = '<tr><th>' + no + '</th>';
                        html += '<td>' + data[i].nama + '</td>';
                        html += '<td>' + data[i].jumlah + '</td>';
                        html += '<td>' + data[i].satuan + '</td>';
                        html += '<td><button id="' + data[i].id + '" onclick="Delete(' + data[i].id + ')" class="btn btn-danger"><i class="fas fa-trash"></i></button></td></tr>';
                        $('#table tbody').append(html);
                    }
                }
            })
        }

        $('#form_Add').on('submit', (e) => {
            e.preventDefault();
            if ($('#save').val() == 'save') {
                $.ajax({
                    url: "<?= base_url('pegawaigudang/barangmasuk/addbarang/') ?>",
                    type: 'POST',
                    data: {
                        'id_barang': $('#id_barang').val(),
                        'jumlah': $('#jumlah').val()
                    },
                    dataType: "json",
                    success: (data) => {
                        $('#jumlah').val('1')
                        $('#save').val('add')
                        $('#id_bmasuk').val(data.insert_id)
                        table()
                    }
                })
            } else if ($('#save').val() == 'add') {
                $.ajax({
                    url: "<?= base_url('pegawaigudang/barangmasuk/addbarangnext/') ?>",
                    type: 'POST',
                    data: {
                        'id_bmasuk': $('#id_bmasuk').val(),
                        'id_barang': $('#id_barang').val(),
                        'jumlah': $('#jumlah').val()
                    },
                    dataType: "json",
                    success: (data) => {
                        $('#jumlah').val('1')
                        $('#table tbody tr').remove();
                        table()
                    }
                })
            }
        })
    })
</script>