<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Barang Rusak</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?= base_url('pegawaigudang/home'); ?>">Home</a></li>
                    <li class="breadcrumb-item"><a href="<?= base_url('pegawaigudang/barangrusak'); ?>">Barang Rusak</a></li>
                    <li class="breadcrumb-item active">Detail</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <!-- general form elements disabled -->
                <div class="card card-default">
                    <div class="card-header">
                        <h3 class="card-title">Detail Barang Rusak</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <h3>Detail</h3>
                                <table>
                                    <tr>
                                        <td>Tanggal&nbsp;&nbsp; </td>
                                        <td>:&nbsp;&nbsp;</td>
                                        <td><?= $detail->tanggal ?></td>
                                    </tr>
                                    <tr>
                                        <td>Nama Penginput&nbsp;&nbsp;</td>
                                        <td>:&nbsp;&nbsp;</td>
                                        <td><?= $detail->nama_user ?></td>
                                    </tr>
                                    <tr>
                                        <td>Jumlah Barang&nbsp;&nbsp;</td>
                                        <td>:&nbsp;&nbsp;</td>
                                        <td><?= $detail->jumlah_barang ?></td>
                                    </tr>
                                    <tr>
                                        <td>Keterangan&nbsp;&nbsp;</td>
                                        <td>:&nbsp;&nbsp;</td>
                                        <td><?= $detail->keterangan ?></td>
                                    </tr>
                                    <tr>
                                        <td>Status&nbsp;&nbsp;</td>
                                        <td>:&nbsp;&nbsp;</td>
                                        <?php if ($detail->is_accept == 0) : ?>
                                            <td>Proses</td>
                                        <?php elseif ($detail->is_accept == 1) : ?>
                                            <td>Disetujui</td>
                                        <?php else : ?>
                                            <td>Ditolak</td>
                                        <?php endif ?>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-sm-6">
                                <h3>Data Barang</h3>
                                <table id="table" class="table table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Nama</th>
                                            <th scope="col">Jumlah</th>
                                            <th scope="col">Satuan Barang</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no = 1;
                                        foreach ($detail2 as $p) : ?>
                                            <tr>
                                                <td><?= $no++;  ?></td>
                                                <td><?= $p->nama_barang; ?></td>
                                                <td><?= $p->jumlah; ?></td>
                                                <td><?= $p->satuan; ?></td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <?php if ($detail->is_accept == 0) : ?>
                            <a href="<?= base_url('managergudang/retur/accept/' . $detail->id_brusak) ?>" type="button" class="btn btn-success float-left mr-1">Terima</a>
                            <a href="<?= base_url('managergudang/retur/reject/' . $detail->id_brusak) ?>" type="button" class="btn btn-danger float-left">Tolak</a>
                        <?php endif ?>
                        <a href="<?= base_url('managergudang/retur') ?>" type="button" class="btn btn-primary float-right" value="save" id="save">Keluar</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>