<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Retur Barang</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Retur Barang</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <?= $this->session->flashdata('message') ?>
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Retur Barang</h3>
            </div>
            <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Tanggal</th>
                            <th>Nama Penginput</th>
                            <th>Keterangan</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no = 1;
                        foreach ($data as $m) : ?>
                            <tr>
                                <td><?= $no++;  ?></td>
                                <td><?= $m->tanggal; ?></td>
                                <td><?= $m->nama; ?></td>
                                <td><?= $m->keterangan; ?></td>
                                <?php if ($m->is_accept == 0) : ?>
                                    <td>Proses</td>
                                <?php elseif ($m->is_accept == 1) : ?>
                                    <td>Disetujui</td>
                                <?php else : ?>
                                    <td>Ditolak</td>
                                <?php endif ?>
                                <td><a href="<?= base_url('managergudang/retur/detail/') . $m->id ?>" class="btn btn-primary">Detail</a></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                    <tfoot>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</section>