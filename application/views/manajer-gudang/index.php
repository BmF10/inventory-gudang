<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Home</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <!-- <li class="breadcrumb-item active">Lorem</li> -->
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <a href="<?= base_url('laporan/barang') ?>" class="btn btn-primary">Laporan Barang</a>
        <br><br>
        <div class="row">
            <div class="col">
                <h2 align="center">Selamat datang <b><?= $user['nama']; ?></b> !!!</h2>
                <h3 align="center">Apakah ada evaluasi gudang hari ini ???</h3>
            </div>
        </div>
    </div>
</section>