<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Evaluasi Pegawai</h1>

            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Evaluasi</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <?= $this->session->flashdata('message') ?>

        <div class="card">
            <div class="card-header">
                <!-- <h3 class="card-title">DataTable with default features</h3> -->
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalevaluasi">
                    Tambah Evaluasi
                </button>
            </div>
            <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>Tanggal</th>
                            <th>Penjelasan</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no = 1;
                        foreach ($evaluasi as $e) : ?>
                            <tr>
                                <td><?= $no++;  ?></td>
                                <td><?= $e->nama; ?></td>
                                <td><?= $e->tanggal; ?></td>
                                <td><?= substr($e->penjelasan, 0, 40); ?>...</td>
                                <td>
                                    <a href="<?= base_url('managergudang/evaluasi/delete/' . $e->id) ?>" onclick="return confirm('Anda yakin ingin menghapus data ini?');" class="btn btn-danger"> Hapus</a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                    <tfoot>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="modalevaluasi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog  modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambahkan Evaluasi Gudang</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?= base_url('/managergudang/evaluasi/add') ?>" method="POST">
                <div class="modal-body">
                    <input type="hidden" name="id_manager" value="<?= $this->session->user_id ?>">
                    <input type="hidden" name="tanggal" value="<?= date("Y-m-d H:i:s"); ?>">
                    <div class="form-group">
                        <label for="exampleFormControlTextarea1">Penjelasan</label>
                        <textarea placeholder="Tulis Evaluasi Anda..." name="penjelasan" class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>