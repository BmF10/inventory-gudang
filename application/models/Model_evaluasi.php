<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_evaluasi extends CI_Model
{

    function getEvaluasi()
    {
        return $this->db->query("SELECT
evaluasi.*,
user.`nama`
FROM
    `aw`.`evaluasi`
    INNER JOIN `aw`.`user` 
        ON (`evaluasi`.`id_manager` = `user`.`id`);");
    }
}
