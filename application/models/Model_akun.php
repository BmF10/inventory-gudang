<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_akun extends CI_Model {

    public function getAkun()
    {
    	$this->db->where('role_id !=', 1);
        $this->db->order_by('id', 'DESC');
		return $this->db->get('user')->result_array();
    }


}