<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_retur extends CI_Model
{
    function getData()
    {
        return $this->db->query("SELECT
    `brusak`.*
    , `user`.`nama`
FROM
    `brusak`
    INNER JOIN `user` 
        ON (`brusak`.`id_user` = `user`.`id`)
        ORDER BY id DESC");
    }

    function getDetail($id)
    {
        return $this->db->query("SELECT
    `brusak`.*
    , `detail_brusak`.*
    , `produk`.`nama` AS nama_barang
    , `produk`.`satuan`
    , `user`.`nama` AS nama_user
    , COUNT(`detail_brusak`.`id_barang`) AS jumlah_barang
FROM
    `brusak`
    INNER JOIN `detail_brusak` 
        ON (`brusak`.`id` = `detail_brusak`.`id_brusak`)
    LEFT JOIN `user` 
        ON (`brusak`.`id_user` = `user`.`id`)
    LEFT JOIN `produk` 
        ON (`detail_brusak`.`id_barang` = `produk`.`id`)
        WHERE brusak.`id` = '$id' GROUP BY `detail_brusak`.`id`");
    }
}
