<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_bkeluar extends CI_Model
{

    function getData($id_bkeluar)
    {
        return $this->db->query("SELECT
    `detail_bkeluar`.*
    , `produk`.`nama`
    , `produk`.`satuan`
FROM
    `detail_bkeluar`
    INNER JOIN `produk` 
        ON (`detail_bkeluar`.`id_barang` = `produk`.`id`)
        WHERE id_bkeluar = '$id_bkeluar'");
    }

    function getAllData()
    {
        return $this->db->query("SELECT
    `bkeluar`.*
    , `user`.`nama`
FROM
    `bkeluar`
    INNER JOIN `user` 
        ON (`bkeluar`.`id_user` = `user`.`id`)
        ORDER BY id DESC");
    }

    function getDetail($id)
    {
        return $this->db->query("SELECT
    `bkeluar`.*
    , `detail_bkeluar`.*
    , `produk`.`nama` AS nama_barang
    , `produk`.`satuan`
    , `user`.`nama` AS nama_user
    , COUNT(`detail_bkeluar`.`id_barang`) AS jumlah_barang
FROM
    `bkeluar`
    INNER JOIN `detail_bkeluar` 
        ON (`bkeluar`.`id` = `detail_bkeluar`.`id_bkeluar`)
    LEFT JOIN `user` 
        ON (`bkeluar`.`id_user` = `user`.`id`)
    LEFT JOIN `produk` 
        ON (`detail_bkeluar`.`id_barang` = `produk`.`id`)
        WHERE bkeluar.`id` = '$id' GROUP BY `detail_bkeluar`.`id`");
    }
}
