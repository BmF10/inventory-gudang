<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_psupplier extends CI_Model
{

    function getAllData()
    {
        return $this->db->query("SELECT
    `psupplier`.*
    , `user`.`nama`
FROM
    `psupplier`
    INNER JOIN `user` 
        ON (`psupplier`.`id_user` = `user`.`id`)
        ORDER BY id DESC");
    }

    function getData($id_psupplier)
    {
        return $this->db->query("SELECT
    `detail_psupplier`.*
    , `produk`.`nama`
FROM
    `detail_psupplier`
    INNER JOIN `produk` 
        ON (`detail_psupplier`.`id_barang` = `produk`.`id`)
        WHERE id_psupplier = '$id_psupplier'");
    }

    function getPemesanan($id)
    {
        return $this->db->query("SELECT
        `psupplier`.`id` as psupplier_id
    ,`psupplier`.*
    , `user`.`nama` as nama_user
    , `produk`.`nama` as nama_barang
    , `detail_psupplier`.*
    , `detail_psupplier`.jumlah as permintaan
    , `produk`.`jumlah` AS stok
    , `produk`.*
    , COUNT(`detail_psupplier`.`id_barang`) as jumlah_barang
FROM
    `psupplier`
    LEFT JOIN `detail_psupplier` 
        ON (`psupplier`.`id` = `detail_psupplier`.`id_psupplier`)
    LEFT JOIN `produk` 
        ON (`detail_psupplier`.`id_barang` = `produk`.`id`)
    LEFT JOIN `user`
        ON (`user`.`id`=`psupplier`.`id_user`)
        WHERE `psupplier`.`id` = '$id'
        GROUP BY detail_psupplier.`id`
        ");
    }
}
