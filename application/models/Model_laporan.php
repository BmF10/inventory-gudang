<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_laporan extends CI_Model
{

    function getBmasuk()
    {
        $dateNow = date('Y-m-d');
        $dateBefore = date('Y-m-d', strtotime("-1 months"));

        return $this->db->query("SELECT
    `bmasuk`.`tanggal`
    , `detail_bmasuk`.*
    , `produk`.`nama`
    , `produk`.`satuan`
    , `user`.`nama` as `user`
FROM
    `bmasuk`
    INNER JOIN `detail_bmasuk` 
        ON (`bmasuk`.`id` = `detail_bmasuk`.`id_bmasuk`)
    INNER JOIN `produk` 
        ON (`detail_bmasuk`.`id_barang` = `produk`.`id`)
    INNER JOIN `user` 
        ON (`user`.`id` = `bmasuk`.`id_user`)
        WHERE tanggal BETWEEN '$dateBefore' AND '$dateNow' ORDER BY id_bmasuk ASC");
    }

    function getBkeluar()
    {
        $dateNow = date('Y-m-d');
        $dateBefore = date('Y-m-d', strtotime("-1 months"));

        return $this->db->query("SELECT
    `bkeluar`.*
    , `detail_bkeluar`.*
    , `produk`.`nama`
    , `produk`.`satuan`
    , `user`.`nama` as `user`
FROM
    `bkeluar`
    INNER JOIN `detail_bkeluar` 
        ON (`bkeluar`.`id` = `detail_bkeluar`.`id_bkeluar`)
    INNER JOIN `produk` 
        ON (`detail_bkeluar`.`id_barang` = `produk`.`id`)
    INNER JOIN `user` 
        ON (`user`.`id` = `bkeluar`.`id_user`)
        WHERE tanggal BETWEEN '$dateBefore' AND '$dateNow' ORDER BY id_bkeluar ASC");
    }

    function getBrusak()
    {
        $dateNow = date('Y-m-d');
        $dateBefore = date('Y-m-d', strtotime("-1 months"));

        return $this->db->query("SELECT
    `brusak`.*
    , `detail_brusak`.*
    , `produk`.`nama`
    , `produk`.`satuan`
    , `user`.`nama` as `user`
FROM
    `brusak`
    INNER JOIN `detail_brusak` 
        ON (`brusak`.`id` = `detail_brusak`.`id_brusak`)
    INNER JOIN `produk` 
        ON (`detail_brusak`.`id_barang` = `produk`.`id`)
    INNER JOIN `user` 
        ON (`user`.`id` = `brusak`.`id_user`)
        WHERE tanggal BETWEEN '$dateBefore' AND '$dateNow' ORDER BY id_brusak ASC");
    }

    function getPemesananGudang()
    {
        $dateNow = date('Y-m-d');
        $dateBefore = date('Y-m-d', strtotime("-1 months"));

        return $this->db->query("SELECT
        `pgudang`.*
        , `pgudang`.`id` AS pgudang_id
        , `detail_pgudang`.*
        , `produk`.`nama` AS produk
        , `produk`.`satuan`
        , `user`.`nama` AS user
    FROM
        `pgudang`
        INNER JOIN `detail_pgudang` 
            ON (`pgudang`.`id` = `detail_pgudang`.`id_pgudang`)
        INNER JOIN `user` 
            ON (`user`.`id` = `pgudang`.`id_user`)
        INNER JOIN `produk` 
            ON (`produk`.`id` = `detail_pgudang`.`id_barang`)
            WHERE tanggal BETWEEN '$dateBefore' AND '$dateNow' ORDER BY pgudang_id ASC");
    }

    function getPemesananSupplier()
    {
        $dateNow = date('Y-m-d');
        $dateBefore = date('Y-m-d', strtotime("-1 months"));

        return $this->db->query("SELECT
        `psupplier`.*
        , `psupplier`.`id` AS psupplier_id
        , `detail_psupplier`.*
        , `produk`.`nama` AS produk
        , `user`.`nama` AS user
        , `produk`.`satuan`
    FROM
        `psupplier`
        INNER JOIN `detail_psupplier` 
            ON (`psupplier`.`id` = `detail_psupplier`.`id_psupplier`)
        INNER JOIN `user` 
            ON (`user`.`id` = `psupplier`.`id_user`)
        INNER JOIN `produk` 
            ON (`produk`.`id` = `detail_psupplier`.`id_barang`)
            WHERE tanggal BETWEEN '$dateBefore' AND '$dateNow' ORDER BY psupplier_id ASC");
    }
}
