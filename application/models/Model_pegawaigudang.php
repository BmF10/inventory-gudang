<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_pegawaigudang extends CI_Model
{

    function getAllPemesanan()
    {
        return $this->db->query("SELECT
    `pgudang`.*
    , `user`.`nama`
    , `pgudang`.`id` as id_pgudang
FROM
    `pgudang`
    INNER JOIN `user` 
        ON (`pgudang`.`id_user` = `user`.`id`)
        WHERE is_read = '0'
        ORDER BY id ASC");
    }

    function getAllPemesanan1()
    {
        return $this->db->query("SELECT
    `pgudang`.*
    , `user`.`nama`
    , `pgudang`.`id` as id_pgudang
FROM
    `pgudang`
    INNER JOIN `user` 
        ON (`pgudang`.`id_user` = `user`.`id`)
        ORDER BY id ASC");
    }

    function getPemesanan($id)
    {
        return $this->db->query("SELECT
        `pgudang`.`id` as pgudang_id
    ,`pgudang`.*
    , `user`.`nama` as nama_user
    , `produk`.`nama` as nama_barang
    , `produk`.`satuan`
    , `detail_pgudang`.*
    , `detail_pgudang`.jumlah as permintaan
    , `produk`.`jumlah` AS stok
    , `produk`.*
    , COUNT(`detail_pgudang`.`id_barang`) as jumlah_barang
FROM
    `pgudang`
    LEFT JOIN `detail_pgudang` 
        ON (`pgudang`.`id` = `detail_pgudang`.`id_pgudang`)
    LEFT JOIN `produk` 
        ON (`detail_pgudang`.`id_barang` = `produk`.`id`)
    LEFT JOIN `user`
        ON (`user`.`id`=`pgudang`.`id_user`)
        WHERE `pgudang`.`id` = '$id'
        GROUP BY detail_pgudang.`id`
        ");
    }

    function getAllEvaluasi()
    {
        return $this->db->query("SELECT
    `evaluasi`.*
    , `user`.`nama`
FROM
    `aw`.`evaluasi`
    INNER JOIN `aw`.`user` 
        ON (`evaluasi`.`id_manager` = `user`.`id`)
        ORDER BY id DESC;");
    }

    function getEvaluasi($id)
    {
        return $this->db->query("SELECT
    `evaluasi`.*
    , `user`.*
    , `evaluasi`.`id` as evaluasi_id
FROM
    `evaluasi`
    INNER JOIN `user` 
        ON (`evaluasi`.`id_manager` = `user`.`id`)
        WHERE `evaluasi`.`id` = '$id'");
    }
}
