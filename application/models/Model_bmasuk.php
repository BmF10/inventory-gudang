<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_bmasuk extends CI_Model
{

    function getData($id_bmasuk)
    {
        return $this->db->query("SELECT
    `detail_bmasuk`.*
    , `produk`.`nama`
    , `produk`.`satuan`
FROM
    `detail_bmasuk`
    INNER JOIN `produk` 
        ON (`detail_bmasuk`.`id_barang` = `produk`.`id`)
        WHERE id_bmasuk = '$id_bmasuk'");
    }

    function getAllData()
    {
        return $this->db->query("SELECT
    `bmasuk`.*
    , `user`.`nama`
FROM
    `bmasuk`
    INNER JOIN `user` 
        ON (`bmasuk`.`id_user` = `user`.`id`)
        ORDER BY id DESC");
    }

    function getDetail($id)
    {
        return $this->db->query("SELECT
    `bmasuk`.*
    , `detail_bmasuk`.*
    , `produk`.`nama` AS nama_barang
    , `produk`.`satuan`
    , `user`.`nama` AS nama_user
    , COUNT(`detail_bmasuk`.`id_barang`) AS jumlah_barang
FROM
    `bmasuk`
    INNER JOIN `detail_bmasuk` 
        ON (`bmasuk`.`id` = `detail_bmasuk`.`id_bmasuk`)
    LEFT JOIN `user` 
        ON (`bmasuk`.`id_user` = `user`.`id`)
    LEFT JOIN `produk` 
        ON (`detail_bmasuk`.`id_barang` = `produk`.`id`)
        WHERE bmasuk.`id` = '$id' GROUP BY `detail_bmasuk`.`id`");
    }
}
