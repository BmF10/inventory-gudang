<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_pgudang extends CI_Model
{

    function getAllData()
    {
        return $this->db->query("SELECT
    `pgudang`.*
    , `user`.`nama`
FROM
    `pgudang`
    INNER JOIN `user` 
        ON (`pgudang`.`id_user` = `user`.`id`)
        ORDER BY id DESC");
    }

    function getData($id_pgudang)
    {
        return $this->db->query("SELECT
    `detail_pgudang`.*
    , `produk`.`nama`
    , `produk`.`satuan`
FROM
    `detail_pgudang`
    INNER JOIN `produk` 
        ON (`detail_pgudang`.`id_barang` = `produk`.`id`)
        WHERE id_pgudang = '$id_pgudang'");
    }

    function getPemesanan($id)
    {
        return $this->db->query("SELECT
        `pgudang`.`id` as pgudang_id
    ,`pgudang`.*
    , `user`.`nama` as nama_user
    , `produk`.`nama` as nama_barang
    , `detail_pgudang`.*
    , `detail_pgudang`.jumlah as permintaan
    , `produk`.`jumlah` AS stok
    , `produk`.*
    , COUNT(`detail_pgudang`.`id_barang`) as jumlah_barang
FROM
    `pgudang`
    LEFT JOIN `detail_pgudang` 
        ON (`pgudang`.`id` = `detail_pgudang`.`id_pgudang`)
    LEFT JOIN `produk` 
        ON (`detail_pgudang`.`id_barang` = `produk`.`id`)
    LEFT JOIN `user`
        ON (`user`.`id`=`pgudang`.`id_user`)
        WHERE `pgudang`.`id` = '$id'
        GROUP BY detail_pgudang.`id`
        ");
    }
}
