<?php

function is_logged_in()
{
	$ci = get_instance();
	if (!$ci->session->userdata('username')) {
		redirect('auth');
	}
}

function mu_only()
{
	$ci = get_instance();
	if ($ci->session->userdata('role_id') == 2) { // 
		redirect('managergudang/home');
	} elseif ($ci->session->userdata('role_id') == 3) {
		redirect('pegawaigudang/home');
	}
}

function mg_only()
{
	$ci = get_instance();
	if ($ci->session->userdata('role_id') == 1) { // 
		redirect('managerutama/home');
	} elseif ($ci->session->userdata('role_id') == 3) {
		redirect('pegawaigudang/home');
	}
}

function pg_only()
{
	$ci = get_instance();
	if ($ci->session->userdata('role_id') == 1) { // 
		redirect('managerutama/home');
	} elseif ($ci->session->userdata('role_id') == 2) {
		redirect('managergudang/home');
	}
}
